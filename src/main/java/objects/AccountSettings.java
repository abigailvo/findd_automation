package objects;

public class AccountSettings {
	private BiometricSettings BiometricSettings;

	private ApiSettings ApiSettings;

	private DataRetentionSettings DataRetentionSettings;

	private String PushAllUsersToClock;

	private String AdminEmail;

	private String ContactName;

	private String ContactEmail;

	private LocalizationSettings LocalizationSettings;

	private AccountStats AccountStats;

	private LocationSettings LocationSettings;

	private String ExceptionEmailList;

	public BiometricSettings getBiometricSettings() {
		return BiometricSettings;
	}

	public void setBiometricSettings(BiometricSettings BiometricSettings) {
		this.BiometricSettings = BiometricSettings;
	}

	public ApiSettings getApiSettings() {
		return ApiSettings;
	}

	public void setApiSettings(ApiSettings ApiSettings) {
		this.ApiSettings = ApiSettings;
	}

	public DataRetentionSettings getDataRetentionSettings() {
		return DataRetentionSettings;
	}

	public void setDataRetentionSettings(DataRetentionSettings DataRetentionSettings) {
		this.DataRetentionSettings = DataRetentionSettings;
	}

	public String getPushAllUsersToClock() {
		return PushAllUsersToClock;
	}

	public void setPushAllUsersToClock(String PushAllUsersToClock) {
		this.PushAllUsersToClock = PushAllUsersToClock;
	}

	public String getAdminEmail() {
		return AdminEmail;
	}

	public void setAdminEmail(String AdminEmail) {
		this.AdminEmail = AdminEmail;
	}

	public String getContactName() {
		return ContactName;
	}

	public void setContactName(String ContactName) {
		this.ContactName = ContactName;
	}

	public String getContactEmail() {
		return ContactEmail;
	}

	public void setContactEmail(String ContactEmail) {
		this.ContactEmail = ContactEmail;
	}

	public LocalizationSettings getLocalizationSettings() {
		return LocalizationSettings;
	}

	public void setLocalizationSettings(LocalizationSettings LocalizationSettings) {
		this.LocalizationSettings = LocalizationSettings;
	}

	public AccountStats getAccountStats() {
		return AccountStats;
	}

	public void setAccountStats(AccountStats AccountStats) {
		this.AccountStats = AccountStats;
	}

	public LocationSettings getLocationSettings() {
		return LocationSettings;
	}

	public void setLocationSettings(LocationSettings LocationSettings) {
		this.LocationSettings = LocationSettings;
	}

	public String getExceptionEmailList() {
		return ExceptionEmailList;
	}

	public void setExceptionEmailList(String ExceptionEmailList) {
		this.ExceptionEmailList = ExceptionEmailList;
	}

	@Override
	public String toString() {
		return "ClassPojo [BiometricSettings = " + BiometricSettings + ", ApiSettings = " + ApiSettings
				+ ", DataRetentionSettings = " + DataRetentionSettings + ", PushAllUsersToClock = "
				+ PushAllUsersToClock + ", AdminEmail = " + AdminEmail + ", ContactName = " + ContactName
				+ ", ContactEmail = " + ContactEmail + ", LocalizationSettings = " + LocalizationSettings
				+ ", AccountStats = " + AccountStats + ", LocationSettings = " + LocationSettings
				+ ", ExceptionEmailList = " + ExceptionEmailList + "]";
	}
}
