package objects;

public class LocalizationSettings {
	private String CurrencyFormat;

	private String DateTimeFormat;

	private String[] TimeZoneIdList;

	private String DefaultTimezoneId;

	private String LanguageId;

	public String getCurrencyFormat() {
		return CurrencyFormat;
	}

	public void setCurrencyFormat(String CurrencyFormat) {
		this.CurrencyFormat = CurrencyFormat;
	}

	public String getDateTimeFormat() {
		return DateTimeFormat;
	}

	public void setDateTimeFormat(String DateTimeFormat) {
		this.DateTimeFormat = DateTimeFormat;
	}

	public String[] getTimeZoneIdList() {
		return TimeZoneIdList;
	}

	public void setTimeZoneIdList(String[] TimeZoneIdList) {
		this.TimeZoneIdList = TimeZoneIdList;
	}

	public String getDefaultTimezoneId() {
		return DefaultTimezoneId;
	}

	public void setDefaultTimezoneId(String DefaultTimezoneId) {
		this.DefaultTimezoneId = DefaultTimezoneId;
	}

	public String getLanguageId() {
		return LanguageId;
	}

	public void setLanguageId(String LanguageId) {
		this.LanguageId = LanguageId;
	}

	@Override
	public String toString() {
		return "ClassPojo [CurrencyFormat = " + CurrencyFormat + ", DateTimeFormat = " + DateTimeFormat
				+ ", TimeZoneIdList = " + TimeZoneIdList + ", DefaultTimezoneId = " + DefaultTimezoneId
				+ ", LanguageId = " + LanguageId + "]";
	}
}
