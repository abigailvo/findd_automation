package objects;

import java.util.List;

public class UserRole {
	private String Type;

	private List<String> Permissions;

	public String getType() {
		return Type;
	}

	public void setType(String Type) {
		this.Type = Type;
	}

	public List<String> getPermissions() {
		return Permissions;
	}

	public void setPermissions(List<String> Permissions) {
		this.Permissions = Permissions;
	}

	@Override
	public String toString() {
		return "ClassPojo [Type = " + Type + ", Permissions = " + Permissions + "]";
	}
}
