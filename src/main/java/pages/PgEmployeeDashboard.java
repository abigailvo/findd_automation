package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import web.functions.GeneralMethods;

public class PgEmployeeDashboard {
	public static WebElement txtFirstName() {
		return GeneralMethods.getElement(By.xpath("//input[@id='firstName']"));
	}
	
	public static WebElement txtLastName() {
		return GeneralMethods.getElement(By.xpath("//input[@id='lastName']"));
	}
	
	public static WebElement txtExternalID() {
		return GeneralMethods.getElement(By.xpath("//input[@id='externalId']"));
	}
	
	public static WebElement txtClockPin() {
		return GeneralMethods.getElement(By.xpath("//input[@id='clockPin']"));
	}
	
	public static WebElement txtTrainedDate() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'TrainedDate')]"));
	}
	
	public static WebElement tabRecords(){
		return GeneralMethods.getElement(By.xpath("//div[contains(.,'records')]/parent::button"));
	}
	
	public static WebElement tabLocations(){
		return GeneralMethods.getElement(By.xpath("//div[contains(.,'locations')]/parent::button"));
	}
	
	public static WebElement tabDevices(){
		return GeneralMethods.getElement(By.xpath("//div[contains(.,'devices')]/parent::button"));
	}
	
	public static WebElement tabFacepaces(){
		return GeneralMethods.getElement(By.xpath("//div[contains(.,'face spaces')]/parent::button"));
	}
	
	/** tab records' controls **/
	public static WebElement rowRecord(String clockpin) {
		return GeneralMethods.getElement(By.xpath("//td[.='" + clockpin + "']/parent::tr"));
	}
	
	/** end tab records' controls **/
	
	/** tab locations' controls **/
	/** end tab locations' controls **/
	
	/** tab devices' controls **/
	/** end tab devices' controls **/
	
	/** tab face paces' controls **/
	/** end face paces' controls **/
}
