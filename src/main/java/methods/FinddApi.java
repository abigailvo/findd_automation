package methods;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import objects.Location;
import web.functions.GeneralMethods;

public class FinddApi {

	public static String getResponseFromGET(String URL) {
		StringBuilder sb = new StringBuilder();
		URLConnection urlConn = null;
		InputStreamReader in = null;
		try {

			URL url = new URL(URL);
			urlConn = url.openConnection();
			urlConn.addRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 6.2; WOW64; Trident/7.0; rv:11.0) like Gecko");

			if (urlConn != null) {
				urlConn.setReadTimeout(60 * 1000);
			}
			if (urlConn != null && urlConn.getInputStream() != null) {
				in = new InputStreamReader(urlConn.getInputStream(), Charset.defaultCharset());
				BufferedReader bufferedReader = new BufferedReader(in);
				if (bufferedReader != null) {
					int cp;
					while ((cp = bufferedReader.read()) != -1) {
						sb.append((char) cp);
					}
					bufferedReader.close();
				}
			}
			in.close();
		} catch (Exception e) {
			throw new RuntimeException("Exception while calling URL: " + URL, e);
		}
		return sb.toString();
	}

	public static HttpResponse getResponseFromGET(String URL, String cookies) {
		String url = URL;
		StringBuffer result = new StringBuffer();
		int i = 0;
		HttpResponse response = null;
		try {
			do {
				HttpClient client = new DefaultHttpClient();
				HttpGet httpGet = new HttpGet(url);

				httpGet.setHeader("Accept-Encoding", "gzip, deflate, br");
				httpGet.setHeader("Cookie", cookies);
				httpGet.setHeader("User-Agent",
						"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:49.0) Gecko/20100101 Firefox/49.0");
				response = client.execute(httpGet);
				i++;
			} while (i <= 3);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public static HttpResponse getResponseFromPOST(String apiURL, String json) {
		String url = apiURL;
		StringBuffer result = new StringBuffer();
		HttpResponse response = null;
		try {
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);
			// post.setHeader("Accept-Encoding", "gzip, deflate, br");
			post.setHeader("Content-Type", "application/json");

			int timeout = 60;
			org.apache.http.params.HttpParams httpParams = client.getParams();
			httpParams.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, timeout * 1000);
			httpParams.setParameter(CoreConnectionPNames.SO_TIMEOUT, timeout * 1000);

			StringEntity input = new StringEntity(json);
			input.setContentType("application/json");
			post.setEntity(input);

			response = client.execute(post);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public static String extractJSONFromResponse(HttpResponse response) {
		StringBuffer json = new StringBuffer();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			String line = "";
			while ((line = rd.readLine()) != null) {
				json.append(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json.toString();
	}

	/**
	 * Send request to server and get response back
	 * 
	 * @param apiUrl
	 * @param json
	 *            can be empty
	 * @param authorization
	 * @return HTTPResponse
	 */
	public static HttpResponse getResponseFromPOST(String apiUrl, String json, String authorization) {
		String url = apiUrl;
		StringBuffer result = new StringBuffer();
		HttpResponse response = null;
		try {
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);
			post.setHeader("Content-Type", "application/json");
			post.setHeader("Cookie", authorization);

			int timeout = 60;
			org.apache.http.params.HttpParams httpParams = client.getParams();
			httpParams.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, timeout * 1000);
			httpParams.setParameter(CoreConnectionPNames.SO_TIMEOUT, timeout * 1000);

			if (!json.equals("")) {
				StringEntity input = new StringEntity(json);
				input.setContentType("application/json");
				post.setEntity(input);
			}

			response = client.execute(post);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public static HttpResponse getResponseFromPATCH(String apiUrl, String json, String authorization) {
		String url = apiUrl;
		StringBuffer result = new StringBuffer();
		HttpResponse response = null;
		try {
			HttpClient client = new DefaultHttpClient();
			HttpPatch patch = new HttpPatch(url);
			patch.setHeader("Content-Type", "application/json");
			patch.setHeader("Cookie", authorization);

			int timeout = 60;
			org.apache.http.params.HttpParams httpParams = client.getParams();
			httpParams.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, timeout * 1000);
			httpParams.setParameter(CoreConnectionPNames.SO_TIMEOUT, timeout * 1000);

			StringEntity input = new StringEntity(json);
			input.setContentType("application/json");
			patch.setEntity(input);

			response = client.execute(patch);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public static HttpResponse getResponseFromPACHT(String apiUrl, String json, String authorization) {
		String url = apiUrl;
		StringBuffer result = new StringBuffer();
		HttpResponse response = null;
		try {
			HttpClient client = new DefaultHttpClient();
			HttpPatch patch = new HttpPatch(url);
			patch.setHeader("Content-Type", "application/json");
			patch.setHeader("Cookie", authorization);

			int timeout = 60;
			org.apache.http.params.HttpParams httpParams = client.getParams();
			httpParams.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, timeout * 1000);
			httpParams.setParameter(CoreConnectionPNames.SO_TIMEOUT, timeout * 1000);

			StringEntity input = new StringEntity(json);
			input.setContentType("application/json");
			patch.setEntity(input);

			response = client.execute(patch);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public static HttpResponse getResponseFromPUT(String apiUrl, String json, String authorization) {
		String url = apiUrl;
		StringBuffer result = new StringBuffer();
		HttpResponse response = null;
		try {
			HttpClient client = new DefaultHttpClient();
			HttpPut put = new HttpPut(url);
			put.setHeader("Content-Type", "application/json");
			put.setHeader("Cookie", authorization);

			int timeout = 60;
			org.apache.http.params.HttpParams httpParams = client.getParams();
			httpParams.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, timeout * 1000);
			httpParams.setParameter(CoreConnectionPNames.SO_TIMEOUT, timeout * 1000);

			StringEntity input = new StringEntity(json);
			input.setContentType("application/json");
			put.setEntity(input);

			response = client.execute(put);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public static HttpResponse getResponseFromDELETE(String apiURL, String authorization) {
		String url = apiURL;
		HttpResponse response = null;
		try {
			HttpClient client = new DefaultHttpClient();
			HttpDelete delete = new HttpDelete(url);

			delete.setHeader("Accept-Encoding", "gzip, deflate, br");
			delete.setHeader("Cookie", authorization);
			delete.setHeader("User-Agent",
					"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:49.0) Gecko/20100101 Firefox/49.0");
			response = client.execute(delete);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public static void getAllHeaders(HttpResponse response) {
		// get all headers
		Header[] headers = response.getAllHeaders();
		for (Header header : headers) {
			System.out.println("Key : " + header.getName() + " ,Value : " + header.getValue());
		}
	}

	public static String getCookiesFromResponse(HttpResponse response) {
		return response.getHeaders("Set-Cookie").length == 0 ? "" : response.getHeaders("Set-Cookie")[0].getValue();
	}

	public static String getStatusCodeFromResponse(HttpResponse response) {
		return response.getStatusLine().getStatusCode() + "";
	}

	public static <any> Object convertJSONToObject(String json, Class<?> className) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		Object object = gson.fromJson(json, className);
		return object;
	}

	public static <T> List<T> convertJsonToObjectArray(String json, Class<T[]> className) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		// Type type = (Type) new TypeToken<List<T>>() {}.getType();
		// List<T> typeList = gson.fromJson(json, type);
		// return typeList;
		T[] arr = new Gson().fromJson(json, className);
		return Arrays.asList(arr);
	}

	public static String convertObjectToJSON(Object object, Class<?> className) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(object, className);

		return json;
	}

	public static String generateGUID() {
		int len = 32;
		String randomString = GeneralMethods.randomStringAndDigits(len).toLowerCase();
		// System.out.println(randomString);
		String regex = "^(.{8})(.{4})(.{4})(.{4})(.{12})$";
		String subst = "$1-$2-$3-$4-$5";

		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(randomString);

		// The substituted value will be contained in the result variable
		return matcher.replaceFirst(subst);
	}

	/**
	 * Get first 8 character from GUID
	 * 
	 * @param guid
	 * @return
	 */
	public static String randomStringFromGUID(String guid) {
		return guid.substring(0, 8);
	}

	public static String getCurrentDateWithFinddFormat() {
		return GeneralMethods.getCurrentTime("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	}
}