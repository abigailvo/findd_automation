package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import web.functions.GeneralMethods;

public class PgLocationsAddNew {
	
	public static WebElement txtLocationName() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'LocationName')]"));
	}
	
	public static WebElement txtAddress() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'Streetaddress')]"));
	}
	
	public static WebElement txtApartment() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'Apartmentsuiteunitbuildingflooretc')]"));
	}
	
	public static WebElement btnAddAnother() {
		return GeneralMethods.getElement(By.xpath("//span[text()='Add Another']//ancestor::button[1]"));
	}
	
	public static WebElement btnAddLocation() {
		return GeneralMethods.getElement(By.xpath("//span[text()='Add Location']//ancestor::button[1]"));
	}
	
	public static WebElement btnCancel() {
		return GeneralMethods.getElement(By.xpath("//span[text()='Cancel']//ancestor::button[1]"));
	}
	
}
