package objects;

import java.util.List;

import initiations.Constants;
import methods.FinddApi;
import web.functions.GeneralMethods;

public class Device {

	public Device(String locationId) {
		this.Id = FinddApi.generateGUID();
		// get 8 first characters
		String randomString = FinddApi.randomStringFromGUID(this.Id);
		this.Description = "Description " + randomString;
		this.AccountUserId = null;
		this.Type = Constants.DeviceType.UNKNOWN;
		this.UserId = null;
		this.Name = "SamsungTab";
		this.SerialNumber = null;
		this.RegistrationCode = "E8E4C2";
		this.LocationId = locationId;
		this.AuthenticationToken = "836E58056003C0E28F5DE76442D32E39FD3557B66FB9D8AD8A7EA4AC0504371A76A593"
				+ "81F61F986FB8AD6CDA33DCE7A4CE89DA00D7073793"
				+ "CC8BB8AB0A669FE4DDBA1C45AE5D5F91F6CB455FBB70C9685FBB29AB35692DE5FA47F8A445EBF0329983AB4C4AB950C96B77"
				+ "6F783F8C0576209075CDA286456F58AE528AFA9AAD23AB9AFAA16E400816C85184FF3BFC01E7ED4E568E6B4CB5728FC81459"
				+ "5D0F61C7B70547FC6FC73ABE942139CB087B9C701BA940DD3BD3CC01B0040D75A32692A502B32BF2A4FCEC94C226B3187044"
				+ "0EB87D0E6AD0C6EFBE6D63100446F0183C6ED10799066621D4A4CCB462D810A7FA6A72915EE2B0C081B57591518978739DE3"
				+ "9F56300102666B9ABEB6E00E920EFD32266DF34880299C7DFE32001D0D527972FD725C626248BFD5ED4ECA7C0D5B142CCEB89B"
				+ "5567E5A088ECA62D3A201CFE0DAE7F5A1D348CEABA09E0898E6287FCF9";
		this.AccountId = null;
		this.HardwareDescription = "Hardware Description " + randomString;
		this.ActivationDate = GeneralMethods.getCurrentTime("dd/MM/yyyy");
		this.Logs = null;
		this.Commands = null;
	}

	private List<Logs> Logs;

	private String Description;

	private String AccountUserId;

	private String Type;

	private String UserId;

	private List<Commands> Commands;

	private String Name;

	private String SerialNumber;

	private String RegistrationCode;

	private String LocationId;

	private String AuthenticationToken;

	private String AccountId;

	private String HardwareDescription;

	private String Id;

	private String ActivationDate;

	public List<Logs> getLogs() {
		return Logs;
	}

	public void setLogs(List<Logs> Logs) {
		this.Logs = Logs;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String Description) {
		this.Description = Description;
	}

	public String getAccountUserId() {
		return AccountUserId;
	}

	public void setAccountUserId(String AccountUserId) {
		this.AccountUserId = AccountUserId;
	}

	public String getType() {
		return Type;
	}

	public void setType(String Type) {
		this.Type = Type;
	}

	public String getUserId() {
		return UserId;
	}

	public void setUserId(String UserId) {
		this.UserId = UserId;
	}

	public List<Commands> getCommands() {
		return Commands;
	}

	public void setCommands(List<Commands> Commands) {
		this.Commands = Commands;
	}

	public String getName() {
		return Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}

	public String getSerialNumber() {
		return SerialNumber;
	}

	public void setSerialNumber(String SerialNumber) {
		this.SerialNumber = SerialNumber;
	}

	public String getRegistrationCode() {
		return RegistrationCode;
	}

	public void setRegistrationCode(String RegistrationCode) {
		this.RegistrationCode = RegistrationCode;
	}

	public String getLocationId() {
		return LocationId;
	}

	public void setLocationId(String LocationId) {
		this.LocationId = LocationId;
	}

	public String getAuthenticationToken() {
		return AuthenticationToken;
	}

	public void setAuthenticationToken(String AuthenticationToken) {
		this.AuthenticationToken = AuthenticationToken;
	}

	public String getAccountId() {
		return AccountId;
	}

	public void setAccountId(String AccountId) {
		this.AccountId = AccountId;
	}

	public String getHardwareDescription() {
		return HardwareDescription;
	}

	public void setHardwareDescription(String HardwareDescription) {
		this.HardwareDescription = HardwareDescription;
	}

	public String getId() {
		return Id;
	}

	public void setId(String Id) {
		this.Id = Id;
	}

	public String getActivationDate() {
		return ActivationDate;
	}

	public void setActivationDate(String ActivationDate) {
		this.ActivationDate = ActivationDate;
	}

	@Override
	public String toString() {
		return "ClassPojo [Logs = " + Logs + ", Description = " + Description + ", AccountUserId = " + AccountUserId
				+ ", Type = " + Type + ", UserId = " + UserId + ", Commands = " + Commands + ", Name = " + Name
				+ ", SerialNumber = " + SerialNumber + ", RegistrationCode = " + RegistrationCode + ", LocationId = "
				+ LocationId + ", AuthenticationToken = " + AuthenticationToken + ", AccountId = " + AccountId
				+ ", HardwareDescription = " + HardwareDescription + ", Id = " + Id + ", ActivationDate = "
				+ ActivationDate + "]";
	}
}
