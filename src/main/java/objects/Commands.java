package objects;

import web.functions.GeneralMethods;

public class Commands {

	public Commands() {
		String randomString = ""; 
		this.Id = "";
		this.Command = "Command " + randomString;
		this.ExecutedDate = GeneralMethods.getCurrentTime("yy/MM/yyyy"); //2016-10-16T06:32:28.3164732-06:00
		this.CreationDate = GeneralMethods.getCurrentTime("yy/MM/yyyy");
		this.Executed = "Executed " + randomString;
	}

	private String Command;

	private String ExecutedDate;

	private String CreationDate;

	private String Id;

	private String Executed;

	public String getCommand() {
		return Command;
	}

	public void setCommand(String Command) {
		this.Command = Command;
	}

	public String getExecutedDate() {
		return ExecutedDate;
	}

	public void setExecutedDate(String ExecutedDate) {
		this.ExecutedDate = ExecutedDate;
	}

	public String getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(String CreationDate) {
		this.CreationDate = CreationDate;
	}

	public String getId() {
		return Id;
	}

	public void setId(String Id) {
		this.Id = Id;
	}

	public String getExecuted() {
		return Executed;
	}

	public void setExecuted(String Executed) {
		this.Executed = Executed;
	}

	@Override
	public String toString() {
		return "ClassPojo [Command = " + Command + ", ExecutedDate = " + ExecutedDate + ", CreationDate = "
				+ CreationDate + ", Id = " + Id + ", Executed = " + Executed + "]";
	}
}
