package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import web.functions.GeneralMethods;

public class PgLocations {
	
	public static WebElement btnAddLocation() {
		return GeneralMethods.getElement(By.xpath("//div[contains(@class,'add')]/button"));
	}
	
	public static WebElement btnSearch() {
		return GeneralMethods.getElement(By.xpath("//i[contains(@class,'search itemActionButton')]"));
	}
	
	public static WebElement btnList() {
		return GeneralMethods.getElement(By.xpath("//span[text()='List']//ancestor::button[1]"));
	}
	
	public static WebElement btnMap() {
		return GeneralMethods.getElement(By.xpath("//span[text()='Map']//ancestor::button[1]"));
	}

}
