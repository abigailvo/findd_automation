package methods;

import java.util.List;

import objects.Device;
import objects.Logs;
import web.functions.VerifyMethods;

public class ActDevices {

	public static void verifyEquals(Device actual, Device expected) {

		VerifyMethods.verifyTextEquals(actual.getId(), expected.getId(), "Verify Device.id");
		VerifyMethods.verifyTextEquals(actual.getDescription(), expected.getDescription(), "Verify Device.Description");
		VerifyMethods.verifyTextEquals(actual.getAccountUserId(), expected.getAccountUserId(),
				"Verify Device.AccountUserId");
		VerifyMethods.verifyTextEquals(actual.getType(), expected.getType(), "Verify Device.Type");
		VerifyMethods.verifyTextEquals(actual.getUserId(), expected.getUserId(), "Verify Device.UserId");
		VerifyMethods.verifyTextEquals(actual.getName(), expected.getName(), "Verify Device.Name");
		VerifyMethods.verifyTextEquals(actual.getSerialNumber(), expected.getSerialNumber(),
				"Verify Device.SerialNumber");
		VerifyMethods.verifyTextEquals(actual.getRegistrationCode(), expected.getRegistrationCode(),
				"Verify Device.RegistrationCode");
		VerifyMethods.verifyTextEquals(actual.getLocationId(), expected.getLocationId(), "Verify Device.LocationId");
		VerifyMethods.verifyTextEquals(actual.getAuthenticationToken(), expected.getAuthenticationToken(),
				"Verify Device.AuthenticationToken");
		VerifyMethods.verifyTextEquals(actual.getAccountId(), expected.getAccountId(), "Verify Device.AccountId");
		VerifyMethods.verifyTextEquals(actual.getHardwareDescription(), expected.getHardwareDescription(),
				"Verify Device.HardwareDescription");
		VerifyMethods.verifyTextEquals(actual.getActivationDate(), expected.getActivationDate(),
				"Verify Device.ActivationDate");

		ActLogs.verifyEquals(actual.getLogs(), expected.getLogs());
		ActCommands.verifyEquals(actual.getCommands(), expected.getCommands());
	}

	public static void verifyEquals(List<Device> actual, List<Device> expected) {
		if (actual == null && expected != null) {
			VerifyMethods.verifyTextEquals("null", "not null");
			return;
		}

		if (actual != null && expected == null) {
			VerifyMethods.verifyTextEquals("not null", "null");
			return;
		}

		if (actual == null && expected == null) {
			VerifyMethods.verifyTextEquals("null", "null");
			return;
		}

		for (int i = 0; i < actual.size(); i++) {
			verifyEquals(actual.get(i), expected.get(i));
		}
	}
}
