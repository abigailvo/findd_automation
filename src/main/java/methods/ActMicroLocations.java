package methods;

import java.util.List;

import objects.MicroLocation;
import web.functions.VerifyMethods;

public class ActMicroLocations {

	public static void verifyEquals(MicroLocation actual, MicroLocation expected) {

		VerifyMethods.verifyTextEquals(actual.getId(), expected.getId(), "Verify MicroLocation.");
		VerifyMethods.verifyTextEquals(actual.getName(), expected.getName(), "Verify MicroLocation.");
		VerifyMethods.verifyTextEquals(actual.getLocationId(), expected.getLocationId(),
				"Verify MicroLocation.LocationId");
		VerifyMethods.verifyTextEquals(actual.getAccountId(), expected.getAccountId(),
				"Verify MicroLocation.AccountId");
		VerifyMethods.verifyTextEquals(actual.getBeaconUUID(), expected.getBeaconUUID(),
				"Verify MicroLocation.BeaconUUID");
		VerifyMethods.verifyTextEquals(actual.getActivationCode(), expected.getActivationCode(),
				"Verify MicroLocation.ActivationCode");
		VerifyMethods.verifyTextEquals(actual.getImageUrl(), expected.getImageUrl(), "Verify MicroLocation.ImageUrl");
		VerifyMethods.verifyTextEquals(actual.getFloorPlanId(), expected.getFloorPlanId(), "Verify MicroLocation.");
		VerifyMethods.verifyTextEquals(actual.getBeaconMajorVersion(), expected.getBeaconMajorVersion(),
				"Verify MicroLocation.BeaconMajorVersion");
		VerifyMethods.verifyTextEquals(actual.getBeaconMinorVersion(), expected.getBeaconMinorVersion(),
				"Verify MicroLocation.BeaconMinorVersion");
		VerifyMethods.verifyTextEquals(actual.getFloorPlanLocation_X(), expected.getFloorPlanLocation_X(),
				"Verify MicroLocation.FloorPlanLocation_X");
		VerifyMethods.verifyTextEquals(actual.getFloorPlanLocation_Y(), expected.getFloorPlanLocation_Y(),
				"Verify MicroLocation.FloorPlanLocation_Y");
		VerifyMethods.verifyTextEquals(actual.getTelemetryData(), expected.getTelemetryData(),
				"Verify MicroLocation.TelemetryData");
		VerifyMethods.verifyTextEquals(actual.getBeaconURL(), expected.getBeaconURL(),
				"Verify MicroLocation.BeaconURL");
		VerifyMethods.verifyTextEquals(actual.getActivationDate(), expected.getActivationDate(),
				"Verify MicroLocation.ActivationDate");
		VerifyMethods.verifyTextEquals(actual.getBeaconType(), expected.getBeaconType(),
				"Verify MicroLocation.BeaconType");
		VerifyMethods.verifyEquals(actual.getBeaconEIDs(), expected.getBeaconEIDs(), "Verify MicroLocation.BeaconEIDs");
	}

	public static void verifyEquals(List<MicroLocation> actual, List<MicroLocation> expected) {
		if (actual == null && expected != null) {
			VerifyMethods.verifyTextEquals("null", "not null");
			return;
		}

		if (actual != null && expected == null) {
			VerifyMethods.verifyTextEquals("not null", "null");
			return;
		}

		if (actual == null && expected == null) {
			VerifyMethods.verifyTextEquals("null", "null");
			return;
		}

		for (int i = 0; i < actual.size(); i++) {
			verifyEquals(actual.get(i), expected.get(i));
		}
	}
}
