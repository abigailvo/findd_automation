package objects;

public class Account {
	
	private String Name;

	private String[] Users;

	private BillingInfo BillingInfo;

	private String ExternalId;

	private String[] Locations;

	private Address Address;

	private String Id;

	private String[] AccountUsers;

	private AccountSettings AccountSettings;

	public String getName() {
		return Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}

	public String[] getUsers() {
		return Users;
	}

	public void setUsers(String[] Users) {
		this.Users = Users;
	}

	public BillingInfo getBillingInfo() {
		return BillingInfo;
	}

	public void setBillingInfo(BillingInfo BillingInfo) {
		this.BillingInfo = BillingInfo;
	}

	public String getExternalId() {
		return ExternalId;
	}

	public void setExternalId(String ExternalId) {
		this.ExternalId = ExternalId;
	}

	public String[] getLocations() {
		return Locations;
	}

	public void setLocations(String[] Locations) {
		this.Locations = Locations;
	}

	public Address getAddress() {
		return Address;
	}

	public void setAddress(Address Address) {
		this.Address = Address;
	}

	public String getId() {
		return Id;
	}

	public void setId(String Id) {
		this.Id = Id;
	}

	public String[] getAccountUsers() {
		return AccountUsers;
	}

	public void setAccountUsers(String[] AccountUsers) {
		this.AccountUsers = AccountUsers;
	}

	public AccountSettings getAccountSettings() {
		return AccountSettings;
	}

	public void setAccountSettings(AccountSettings AccountSettings) {
		this.AccountSettings = AccountSettings;
	}

	@Override
	public String toString() {
		return "ClassPojo [Name = " + Name + ", Users = " + Users + ", BillingInfo = " + BillingInfo + ", ExternalId = "
				+ ExternalId + ", Locations = " + Locations + ", Address = " + Address + ", Id = " + Id
				+ ", AccountUsers = " + AccountUsers + ", AccountSettings = " + AccountSettings + "]";
	}
}