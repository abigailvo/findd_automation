package api;

import java.util.List;

import org.apache.http.HttpResponse;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import initiations.Constants;
import methods.ActAccounts;
import methods.ActEmployee;
import methods.ActLocations;
import methods.ActUsers;
import methods.FinddApi;
import objects.Account;
import objects.Location;
import objects.User;
import web.functions.VerifyMethods;

public class VerifyAccountsAPI {

	public String cookie = "";
	public String accountAPI = Constants.ACCOUNT_API;
	public String accountId = Constants.ACCOUNT_ID;

	@BeforeMethod
	public void beforeMethod() {
		cookie = ActEmployee.getCookieFromLoginAPI();
	}

	@Test(priority = 1)
	public void API01_GET_account_by_accountId() {
		String url = accountAPI + accountId;
		HttpResponse response = FinddApi.getResponseFromGET(url, cookie);
		String json = FinddApi.extractJSONFromResponse(response);
		Account accountApi = (Account) FinddApi.convertJSONToObject(json, Location.class);

		// TODO: need to revise by hard code
		Account accountDB = ActAccounts.getAccountFromDb(accountId);

		ActAccounts.verifyEquals(accountApi, accountDB);
		VerifyMethods.verifyTest();
	}

	// accounts/{accountId}/locations
	public void API11_POST_Add_location_to_account() {
		String url = "https://test-api.findd.io/v1/accounts/93ddd288-08c3-439f-8514-4156ed1e4b72/locations";
		Location actual = new Location();
		String json = FinddApi.convertObjectToJSON(actual, Location.class);
		HttpResponse response = FinddApi.getResponseFromPOST(url, json, cookie);
		String result = FinddApi.extractJSONFromResponse(response);
		Location expected = (Location) FinddApi.convertJSONToObject(result, Location.class);
		String code = FinddApi.getStatusCodeFromResponse(response);
		VerifyMethods.verifyTextEquals(code, "200", "Verify Response Code of adding location");
		ActLocations.verifyEquals(actual, expected);
		VerifyMethods.verifyTest();
	}

	public void API04_GET_locations_attached_to_the_account() {
		String url = accountAPI + accountId + "/locations";
		HttpResponse response = FinddApi.getResponseFromGET(url, cookie);
		String json = FinddApi.extractJSONFromResponse(response);
		List<Location> locsAPI = FinddApi.convertJsonToObjectArray(json, Location[].class);
		List<Location> locsDB = (List<Location>) ActLocations.getLocationsFromDb(accountId);
		ActLocations.verifyEquals(locsDB, locsAPI);
		VerifyMethods.verifyTest();
	}

	public void API02_POST_add_user_to_account() {
	}

	// accounts/{accountId}/users
	public void API10_POST_Add_user_to_account() {
		String url = "https://test-api.findd.io/v1/accounts/93ddd288-08c3-439f-8514-4156ed1e4b72/users";
		User actual = new User();
		String json = FinddApi.convertObjectToJSON(actual, User.class);
		HttpResponse response = FinddApi.getResponseFromPOST(url, json, cookie);
		String result = FinddApi.extractJSONFromResponse(response);
		User expected = (User) FinddApi.convertJSONToObject(result, User.class);
		String code = FinddApi.getStatusCodeFromResponse(response);
		VerifyMethods.verifyTextEquals(code, "200", "Verify Response Code of adding user");
		ActUsers.verifyEquals(actual, expected);
		VerifyMethods.verifyTest();
	}

	/**
	 * Get user list with page, limit item per page and search criteria
	 */
	public void API05_GET_users_by_account() {
		// accounts/{accountId}/users?page={page}&limit={limit}&search={search}
	}

	public void API06_GET_users_by_account() {
		// accounts/{accountId}/account_users?page={page}&limit={limit}
	}

	// accounts/{accountId}/account_users?page={page}&limit={limit}
	public void API07_POST_Add_account_user() {

	}

	/**
	 * accounts/{accountId}/records?filterByExceptions={filterByExceptions}
	 * &filterByNotReviewed={filterByNotReviewed}
	 * &filterByNotApproved={filterByNotApproved}
	 * &exceptionTypeFilter={exceptionTypeFilter}&page={page}&limit={limit}
	 * &startDateFilter={startDateFilter} &endDateFilter={endDateFilter}
	 */
	public void API08_GET_account_punch_records() {

	}

	// accounts/{accountId}/records/stats
	public void API09_GET_add_stats_to_account() {

	}

	// accounts/{accountId}/microlocations
	public void AP12_GET_retrieve_all_MicroLocations_assigned_to_account() {

	}

	/**
	 * Call to check whether clock pin is already tied to a user or not within
	 * an account.
	 */
	public void API13_GET_clock_pin_tied_to_account() {
		// accounts/{accountId}/clockPinExists?clockPin={clockPin}
	}

	public void API03_PATCH_update_account_document() {

	}
}
