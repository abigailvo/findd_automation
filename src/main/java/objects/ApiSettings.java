package objects;

public class ApiSettings {
	private String Enabled;

	private String Key;

	public String getEnabled() {
		return Enabled;
	}

	public void setEnabled(String Enabled) {
		this.Enabled = Enabled;
	}

	public String getKey() {
		return Key;
	}

	public void setKey(String Key) {
		this.Key = Key;
	}

	@Override
	public String toString() {
		return "ClassPojo [Enabled = " + Enabled + ", Key = " + Key + "]";
	}
}
