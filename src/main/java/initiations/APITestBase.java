package initiations;

import java.lang.reflect.Method;
import java.net.MalformedURLException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import common.Execution;
import web.Driver;

public class APITestBase {

	@BeforeMethod
	public void beforeBaseMethod(Method method) {
		System.out.println("----------------------\n******** STARTED RUNNING: " + method.getName()
				+ " ********\n----------------------");
		Execution.isPassed = true;
		Execution.method = method;
		Execution.className = this.getClass().getSimpleName();
	}
}