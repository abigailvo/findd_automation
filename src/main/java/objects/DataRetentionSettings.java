package objects;

public class DataRetentionSettings {
	private String NumberOfDaysToKeepServerData;

	private String NumberOfDaysToKeepDeviceData;

	public String getNumberOfDaysToKeepServerData() {
		return NumberOfDaysToKeepServerData;
	}

	public void setNumberOfDaysToKeepServerData(String NumberOfDaysToKeepServerData) {
		this.NumberOfDaysToKeepServerData = NumberOfDaysToKeepServerData;
	}

	public String getNumberOfDaysToKeepDeviceData() {
		return NumberOfDaysToKeepDeviceData;
	}

	public void setNumberOfDaysToKeepDeviceData(String NumberOfDaysToKeepDeviceData) {
		this.NumberOfDaysToKeepDeviceData = NumberOfDaysToKeepDeviceData;
	}

	@Override
	public String toString() {
		return "ClassPojo [NumberOfDaysToKeepServerData = " + NumberOfDaysToKeepServerData
				+ ", NumberOfDaysToKeepDeviceData = " + NumberOfDaysToKeepDeviceData + "]";
	}
}
