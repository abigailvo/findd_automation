package objects;

public class Address {

	private String Phone;

	private String CityTown;

	private String StreetType;

	private String StreetNumber;

	private String PostalArea;

	private String Country;

	private String StreetDirection;

	private String StreetName;

	private String Latitude;

	private String Longitude;

	private String StreetNumberSuffix;

	private String AddressType;

	private String MobilePhone;

	private String HouseOrBuildingName;

	private String GoverningDistrictOrState;

	private String LocalMunicipality;

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String Phone) {
		this.Phone = Phone;
	}

	public String getCityTown() {
		return CityTown;
	}

	public void setCityTown(String CityTown) {
		this.CityTown = CityTown;
	}

	public String getStreetType() {
		return StreetType;
	}

	public void setStreetType(String StreetType) {
		this.StreetType = StreetType;
	}

	public String getStreetNumber() {
		return StreetNumber;
	}

	public void setStreetNumber(String StreetNumber) {
		this.StreetNumber = StreetNumber;
	}

	public String getPostalArea() {
		return PostalArea;
	}

	public void setPostalArea(String PostalArea) {
		this.PostalArea = PostalArea;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String Country) {
		this.Country = Country;
	}

	public String getStreetDirection() {
		return StreetDirection;
	}

	public void setStreetDirection(String StreetDirection) {
		this.StreetDirection = StreetDirection;
	}

	public String getStreetName() {
		return StreetName;
	}

	public void setStreetName(String StreetName) {
		this.StreetName = StreetName;
	}

	public String getLatitude() {
		return Latitude;
	}

	public void setLatitude(String Latitude) {
		this.Latitude = Latitude;
	}

	public String getLongitude() {
		return Longitude;
	}

	public void setLongitude(String Longitude) {
		this.Longitude = Longitude;
	}

	public String getStreetNumberSuffix() {
		return StreetNumberSuffix;
	}

	public void setStreetNumberSuffix(String StreetNumberSuffix) {
		this.StreetNumberSuffix = StreetNumberSuffix;
	}

	public String getAddressType() {
		return AddressType;
	}

	public void setAddressType(String AddressType) {
		this.AddressType = AddressType;
	}

	public String getMobilePhone() {
		return MobilePhone;
	}

	public void setMobilePhone(String MobilePhone) {
		this.MobilePhone = MobilePhone;
	}

	public String getHouseOrBuildingName() {
		return HouseOrBuildingName;
	}

	public void setHouseOrBuildingName(String HouseOrBuildingName) {
		this.HouseOrBuildingName = HouseOrBuildingName;
	}

	public String getGoverningDistrictOrState() {
		return GoverningDistrictOrState;
	}

	public void setGoverningDistrictOrState(String GoverningDistrictOrState) {
		this.GoverningDistrictOrState = GoverningDistrictOrState;
	}

	public String getLocalMunicipality() {
		return LocalMunicipality;
	}

	public void setLocalMunicipality(String LocalMunicipality) {
		this.LocalMunicipality = LocalMunicipality;
	}

	@Override
	public String toString() {
		return "ClassPojo [Phone = " + Phone + ", CityTown = " + CityTown + ", StreetType = " + StreetType
				+ ", StreetNumber = " + StreetNumber + ", PostalArea = " + PostalArea + ", Country = " + Country
				+ ", StreetDirection = " + StreetDirection + ", StreetName = " + StreetName + ", Latitude = " + Latitude
				+ ", Longitude = " + Longitude + ", StreetNumberSuffix = " + StreetNumberSuffix + ", AddressType = "
				+ AddressType + ", MobilePhone = " + MobilePhone + ", HouseOrBuildingName = " + HouseOrBuildingName
				+ ", GoverningDistrictOrState = " + GoverningDistrictOrState + ", LocalMunicipality = "
				+ LocalMunicipality + "]";
	}

	public Address(String uniqueString) {
		this.StreetNumber = "QA Test StreetNumber " + uniqueString;
		this.HouseOrBuildingName = "QA Test HouseOrBuildingName " + uniqueString;
		this.StreetNumberSuffix = "";
		this.StreetName = "QA Test StreetName " + uniqueString;
		this.StreetType = "";
		this.StreetDirection = "";
		this.AddressType = "";
		this.LocalMunicipality = "";
		this.CityTown = "QA Test CityTown " + uniqueString;
		this.GoverningDistrictOrState = "QA Test GoverningDistrictOrState " + uniqueString;
		this.PostalArea = "12345678";
		this.Country = "QA Test Country " + uniqueString;
		this.Phone = "";
		this.MobilePhone = "";
		this.Latitude = "40.272512";
		this.Longitude = "-111.675432";
	}
}
