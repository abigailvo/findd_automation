package methods;

import org.apache.http.HttpResponse;

import pages.PgFinddLogin;
import web.functions.GeneralMethods;

public class ActEmployee {

	public static void login(String email, String password){
		PgFinddLogin.launch();
		GeneralMethods.enter(PgFinddLogin.txtEmail(), email);
		GeneralMethods.sleep(2000);
		GeneralMethods.enter(PgFinddLogin.txtPassword(), password);
		//PgFinddLogin.txtPassword().sendKeys(password);
		PgFinddLogin.btnLogin().click();
		GeneralMethods.waitForPageFullyLoaded(10);
	}
	
	public static String getCookieFromLoginAPI(){
		String url = "https://test-api.findd.io/v1/users/login";
		String json = "{\"Email\":\"upworks.test@testemail.com\",\"Password\":\"password\"}";
		HttpResponse response = FinddApi.getResponseFromPOST(url, json);
		return response.getHeaders("Set-Cookie")[0].getValue();
	}
}
