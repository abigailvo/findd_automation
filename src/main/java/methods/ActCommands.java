package methods;

import java.util.List;

import objects.Commands;
import web.functions.VerifyMethods;

public class ActCommands {

	public static void verifyEquals(Commands actual, Commands expected) {

	}

	public static void verifyEquals(List<Commands> actual, List<Commands> expected) {
		if (actual == null && expected != null) {
			VerifyMethods.verifyTextEquals("null", "not null");
			return;
		}

		if (actual != null && expected == null) {
			VerifyMethods.verifyTextEquals("not null", "null");
			return;
		}

		if (actual == null && expected == null) {
			VerifyMethods.verifyTextEquals("null", "null");
			return;
		}

		for (int i = 0; i < actual.size(); i++) {
			verifyEquals(actual.get(i), expected.get(i));
		}
	}
}
