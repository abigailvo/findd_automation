package objects;

import methods.FinddApi;

public class MicroLocation {

	public MicroLocation(String locationId) {
		Id = FinddApi.generateGUID();
		String randomString = FinddApi.randomStringFromGUID(this.Id);
		Name = "Micro Location Name " + randomString;
		LocationId = locationId;
		AccountId = null;
		BeaconUUID = FinddApi.generateGUID();
		ActivationCode = "C223F";
		ImageUrl = null;
		FloorPlanId = "";
		BeaconMajorVersion = "998";
		BeaconMinorVersion = "1702";
		FloorPlanLocation_Y = "0";
		FloorPlanLocation_X = "0";
		TelemetryData = null;
		BeaconURL = null;
		ActivationDate = FinddApi.getCurrentDateWithFinddFormat();
		BeaconType = null;
		BeaconEIDs = null;
	}

	private String ActivationCode;

	private String ImageUrl;

	private String FloorPlanId;

	private String BeaconMajorVersion;

	private String BeaconMinorVersion;

	private String LocationId;

	private String Name;

	private String BeaconUUID;

	private String FloorPlanLocation_Y;

	private String TelemetryData;

	private String FloorPlanLocation_X;

	private String AccountId;

	private String BeaconURL;

	private String Id;

	private String[] BeaconEIDs;

	private String ActivationDate;

	private String BeaconType;

	public String getActivationCode() {
		return ActivationCode;
	}

	public void setActivationCode(String ActivationCode) {
		this.ActivationCode = ActivationCode;
	}

	public String getImageUrl() {
		return ImageUrl;
	}

	public void setImageUrl(String ImageUrl) {
		this.ImageUrl = ImageUrl;
	}

	public String getFloorPlanId() {
		return FloorPlanId;
	}

	public void setFloorPlanId(String FloorPlanId) {
		this.FloorPlanId = FloorPlanId;
	}

	public String getBeaconMajorVersion() {
		return BeaconMajorVersion;
	}

	public void setBeaconMajorVersion(String BeaconMajorVersion) {
		this.BeaconMajorVersion = BeaconMajorVersion;
	}

	public String getBeaconMinorVersion() {
		return BeaconMinorVersion;
	}

	public void setBeaconMinorVersion(String BeaconMinorVersion) {
		this.BeaconMinorVersion = BeaconMinorVersion;
	}

	public String getLocationId() {
		return LocationId;
	}

	public void setLocationId(String LocationId) {
		this.LocationId = LocationId;
	}

	public String getName() {
		return Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}

	public String getBeaconUUID() {
		return BeaconUUID;
	}

	public void setBeaconUUID(String BeaconUUID) {
		this.BeaconUUID = BeaconUUID;
	}

	public String getFloorPlanLocation_Y() {
		return FloorPlanLocation_Y;
	}

	public void setFloorPlanLocation_Y(String FloorPlanLocation_Y) {
		this.FloorPlanLocation_Y = FloorPlanLocation_Y;
	}

	public String getTelemetryData() {
		return TelemetryData;
	}

	public void setTelemetryData(String TelemetryData) {
		this.TelemetryData = TelemetryData;
	}

	public String getFloorPlanLocation_X() {
		return FloorPlanLocation_X;
	}

	public void setFloorPlanLocation_X(String FloorPlanLocation_X) {
		this.FloorPlanLocation_X = FloorPlanLocation_X;
	}

	public String getAccountId() {
		return AccountId;
	}

	public void setAccountId(String AccountId) {
		this.AccountId = AccountId;
	}

	public String getBeaconURL() {
		return BeaconURL;
	}

	public void setBeaconURL(String BeaconURL) {
		this.BeaconURL = BeaconURL;
	}

	public String getId() {
		return Id;
	}

	public void setId(String Id) {
		this.Id = Id;
	}

	public String[] getBeaconEIDs() {
		return BeaconEIDs;
	}

	public void setBeaconEIDs(String[] BeaconEIDs) {
		this.BeaconEIDs = BeaconEIDs;
	}

	public String getActivationDate() {
		return ActivationDate;
	}

	public void setActivationDate(String ActivationDate) {
		this.ActivationDate = ActivationDate;
	}

	public String getBeaconType() {
		return BeaconType;
	}

	public void setBeaconType(String BeaconType) {
		this.BeaconType = BeaconType;
	}

	@Override
	public String toString() {
		return "ClassPojo [ActivationCode = " + ActivationCode + ", ImageUrl = " + ImageUrl + ", FloorPlanId = "
				+ FloorPlanId + ", BeaconMajorVersion = " + BeaconMajorVersion + ", BeaconMinorVersion = "
				+ BeaconMinorVersion + ", LocationId = " + LocationId + ", Name = " + Name + ", BeaconUUID = "
				+ BeaconUUID + ", FloorPlanLocation_Y = " + FloorPlanLocation_Y + ", TelemetryData = " + TelemetryData
				+ ", FloorPlanLocation_X = " + FloorPlanLocation_X + ", AccountId = " + AccountId + ", BeaconURL = "
				+ BeaconURL + ", Id = " + Id + ", BeaconEIDs = " + BeaconEIDs + ", ActivationDate = " + ActivationDate
				+ ", BeaconType = " + BeaconType + "]";
	}
}
