package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import web.functions.GeneralMethods;

public class PgEmployeeAddNew {
	
	public static WebElement txtFirstName() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'FirstName')]"));
	}
	
	public static WebElement txtLastName() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'LastName')]"));
	}
	
	public static WebElement txtExternalID() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'ID')]"));
	}
	
	public static WebElement txtClockPin() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'ClockPin')]"));
	}
	
	public static WebElement btnAddEmployee() {
		return GeneralMethods.getElement(By.xpath("//div[.='ADD EMPLOYEE']/parent::button"));
	}
	
	public static WebElement btnCancel() {
		return GeneralMethods.getElement(By.xpath("//div[.='CANCEL']/parent::button"));
	}
	
	public static WebElement btnAddAnother() {
		return GeneralMethods.getElement(By.xpath("//div[.='Add Another']/parent::button"));
	}
}
