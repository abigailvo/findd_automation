package methods;

import java.util.List;

import objects.Logs;
import web.functions.VerifyMethods;

public class ActLogs {

	public static void verifyEquals(Logs actual, Logs expected) {

	}

	public static void verifyEquals(List<Logs> actual, List<Logs> expected) {
		if (actual == null && expected != null) {
			VerifyMethods.verifyTextEquals("null", "not null");
			return;
		}

		if (actual != null && expected == null) {
			VerifyMethods.verifyTextEquals("not null", "null");
			return;
		}

		if (actual == null && expected == null) {
			VerifyMethods.verifyTextEquals("null", "null");
			return;
		}

		for (int i = 0; i < actual.size(); i++) {
			verifyEquals(actual.get(i), expected.get(i));
		}
	}
}
