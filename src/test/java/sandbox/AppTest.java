package sandbox;

import java.text.MessageFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.text.MaskFormatter;

import org.apache.http.HttpResponse;
import org.testng.Assert;
import org.testng.annotations.Test;

import initiations.APITestBase;
import initiations.Constants;
import methods.ActEmployee;
import methods.ActLocations;
import methods.FinddApi;
import objects.Location;
import pages.PgEmployeeList;
import web.functions.GeneralMethods;
import web.functions.VerifyMethods;

/**
 * Sample test case to verify framework works correctly
 */
public class AppTest extends APITestBase {

	// @Test
	public void verify_framework_works_correctly() {
		ActEmployee.login("trl@io-web.com", "password");
		PgEmployeeList.launch();
		PgEmployeeList.btnSearch().click();
		GeneralMethods.sleep(10000);
	}

	// @Test
	public void testHttpGet() {
		String cookie = getCookieFromLoginAPI();
		System.out.println(cookie);
		String url = "https://test-api.findd.io/v1/accounts/93ddd288-08c3-439f-8514-4156ed1e4b72/locations";
		HttpResponse response = FinddApi.getResponseFromGET(url, cookie);
		String json = FinddApi.extractJSONFromResponse(response);
		List<Location> locs = FinddApi.convertJsonToObjectArray(json, Location[].class);
		// Gson gson = new GsonBuilder().setPrettyPrinting().create();
		// Type type = (Type) new TypeToken<List<Location>>() {}.getType();
		// List<Location> locs = gson.fromJson(json, type);
		System.out.println(locs.get(0).toString());
	}

	// @Test
	public String getCookieFromLoginAPI() {
		String url = "https://test-api.findd.io/v1/users/login";
		String json = "{\"Email\":\"upworks.test@testemail.com\",\"Password\":\"password\"}";
		HttpResponse response = FinddApi.getResponseFromPOST(url, json);
		return response.getHeaders("Set-Cookie")[0].getValue();
	}

	// @Test(priority = 3)
	public void testGSON() {
		String cookie = getCookieFromLoginAPI();
		String url = "https://test-api.findd.io/v1/accounts/93ddd288-08c3-439f-8514-4156ed1e4b72/locations";
		Location actual = new Location();
		String json = FinddApi.convertObjectToJSON(actual, Location.class);
		System.out.println(json);
		HttpResponse response = FinddApi.getResponseFromPOST(url, json, cookie);
		String result = FinddApi.extractJSONFromResponse(response);
		System.out.println(result);
		Location expected = (Location) FinddApi.convertJSONToObject(result, Location.class);
		String code = FinddApi.getStatusCodeFromResponse(response);
		VerifyMethods.verifyTextEquals(code, "200");
		ActLocations.verifyEquals(actual, expected);
		VerifyMethods.verifyTest();
	}

	// @Test(priority = 2)
	public void verifyTestPassed() {
		// VerifyMethods.verifyTextEquals("1", "1");
		Assert.assertEquals("1", "1");
		// VerifyMethods.verifyTest();
	}

	public void verifyTestFailed() {
		// List<String> abc = new ArrayList<String>();
		// abc.add("Tram");
		// abc.add("Tran");
		VerifyMethods.verifyTest();
	}

	// @Test(priority = 1)
	public void API17_POST_add_user_to_location() {
		String cookie = getCookieFromLoginAPI();
		String locationId = "622197d4-ddd3-b30a-311c-e28afdd7aca4";
		String userId = "bef121a6-e8c9-7177-eae0-b004fd617567";
		String url = Constants.LOCATION_API + "%locationId%/users/add?userId=%userId%";
		url = url.replace("%locationId%", locationId).replaceAll("%userId%", userId);

		HttpResponse response = FinddApi.getResponseFromPOST(url, "", cookie);
		String code = FinddApi.getStatusCodeFromResponse(response);
		VerifyMethods.verifyTextEquals(code, "200", "Verify Response Code of adding user to location");

		// get user to verify if location is associated to user
		String getUserUrl = Constants.USER_API + userId;
		HttpResponse getUserRes = FinddApi.getResponseFromGET(getUserUrl, cookie);
		String getUserJson = FinddApi.extractJSONFromResponse(getUserRes);
		VerifyMethods.verifyTextContains(getUserJson, locationId);

		// TODO After method: remove location from user
		VerifyMethods.verifyTest();
	}

	@Test(priority = 1)
	public void add_new_location(){
		Location location = new Location();
		String cookie = getCookieFromLoginAPI();
		String api = Constants.LOCATION_API + "location";
		String createLocJSON = FinddApi.convertObjectToJSON(location, Location.class);
		HttpResponse response = FinddApi.getResponseFromPOST(api, createLocJSON, cookie);
		String code = FinddApi.getStatusCodeFromResponse(response);

		VerifyMethods.verifyTextEquals(code, "200", "Verify Status of Adding new location");
		VerifyMethods.verifyTest();
	}
	//@Test(priority = 1)
	public void generateGUID() {
		int len = 32;
		String randomString = GeneralMethods.randomStringAndDigits(len).toLowerCase();
		System.out.println(randomString);
		 String regex = "^(.{8})(.{4})(.{4})(.{4})(.{12})$";
		 String subst = "$1-$2-$3-$4-$5";

		 Pattern pattern = Pattern.compile(regex);
		 Matcher matcher = pattern.matcher(randomString);

		// The substituted value will be contained in the result variable
		 String result = matcher.replaceFirst(subst);

		System.out.println("Substitution result: " + result);
	}
	
	//@Test(priority = 1)
	public void formatDate(){
		String time = GeneralMethods.getCurrentTime("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		System.out.println(time);
	}
}
