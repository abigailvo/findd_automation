package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import web.functions.GeneralMethods;

public class PgLocationsDetails {
	
	// LOCATION INFO =============================
	
	public static WebElement lblLocationName() {
		return GeneralMethods.getElement(By.xpath("//h2[@style='text-transform: uppercase;']"));
	}
	
	public static WebElement txtLocationName() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'LocationName')]"));
	}
	
	public static WebElement txtStreet() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'StreetPOcompanyname')]"));
	}
	
	public static WebElement txtApartment() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'Apartmentsuitebuildingflooretc')]"));
	}
	
	public static WebElement txtCity() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'City')]"));
	}
	
	public static WebElement txtState() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'StateProvinceRegion')]"));
	}
	
	public static WebElement txtZIP() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'ZIPPostalcode')]"));
	}
	
	public static WebElement txtCountry() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'Country')]"));
	}
	
	public static WebElement txtLatitude() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'Latitude')]"));
	}
	
	public static WebElement txtLongtitude() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'Longitude')]"));
	}
	
	public static WebElement txtExtID() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'ExternalID')]"));
	}
	
	public static WebElement txtGeoMeter() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'GeofencePerimeter')]"));
	}
	
	// LOCATION TABS =============================
	
	public static WebElement btnEmployees() {
		return GeneralMethods.getElement(By.xpath("//div[text()='Employees']//ancestor::button[1]"));
	}

	public static WebElement btnDevices() {
		return GeneralMethods.getElement(By.xpath("//div[text()='Devices']//ancestor::button[1]"));
	}
	
	public static WebElement btnMicroLocations() {
		return GeneralMethods.getElement(By.xpath("//i[text()='Micro Locations']//ancestor::button[1]"));
	}
	
	public static WebElement btnLinkEmployees() {
		return GeneralMethods.getElement(By.xpath("//i[contains(@class,'fa-link')]//ancestor::button[1]"));
	}
	
	public static WebElement btnAddDevices() {
		return GeneralMethods.getElement(By.xpath("//div[@class='location-detail-tab']//button[1]"));
	}
	
	public static WebElement btnLinkMicroLocations() {
		return GeneralMethods.getElement(By.xpath("//i[contains(@class,'fa-link')]//ancestor::button[1]"));
	}
	
	public static WebElement txtSearch() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'search')]"));
	}
	
	// ADD NEW EMPLOYEE ================================ 
	
	public static WebElement divAddEmployeePopup() {
		return GeneralMethods.getElement(By.xpath("//h3[text()='ADD EMPLOYEE']"));
	}
	
	public static WebElement cbbSelectEmployee() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'undefined')]"));
	}
	
	public static WebElement btnAddEmployee() {
		return GeneralMethods.getElement(By.xpath("//span[text()='Add Employee']//ancestor::button[1]"));
	}
	
	public static WebElement btnAddAnother() {
		return GeneralMethods.getElement(By.xpath("//span[text()='Add Another']//ancestor::button[1]"));
	}
	
	public static WebElement btnCancel() {
		return GeneralMethods.getElement(By.xpath("//span[text()='Cancel']//ancestor::button[1]"));
	}
	
	// LINK NEW DEVICE =================================

	public static WebElement divAddDevicePopup() {
		return GeneralMethods.getElement(By.xpath("//h3[text()='ADD DEVICE']"));
	}
	
	public static WebElement txtName() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'Name')]"));
	}
	
	public static WebElement txtRegistrationCode() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'RegistrationCode')]"));
	}
	
	public static WebElement btnAddDevice() {
		return GeneralMethods.getElement(By.xpath("//span[text()='Add Device']//ancestor::button[1]"));
	}
	
	// ADD NEW MICRO LOCATION=================================

	public static WebElement btnAddMicroLocation() {
		return GeneralMethods.getElement(By.xpath("//span[text()='Add Micro Location']//ancestor::button[1]"));
	}
}
