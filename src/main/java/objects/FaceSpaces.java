package objects;

public class FaceSpaces {
	private String Deleted;

	private String ImageID;

	private String BioTemplateID;

	private String Image;

	private String MatchCount;

	private String Id;

	private String BioTemplate;

	public String getDeleted() {
		return Deleted;
	}

	public void setDeleted(String Deleted) {
		this.Deleted = Deleted;
	}

	public String getImageID() {
		return ImageID;
	}

	public void setImageID(String ImageID) {
		this.ImageID = ImageID;
	}

	public String getBioTemplateID() {
		return BioTemplateID;
	}

	public void setBioTemplateID(String BioTemplateID) {
		this.BioTemplateID = BioTemplateID;
	}

	public String getImage() {
		return Image;
	}

	public void setImage(String Image) {
		this.Image = Image;
	}

	public String getMatchCount() {
		return MatchCount;
	}

	public void setMatchCount(String MatchCount) {
		this.MatchCount = MatchCount;
	}

	public String getId() {
		return Id;
	}

	public void setId(String Id) {
		this.Id = Id;
	}

	public String getBioTemplate() {
		return BioTemplate;
	}

	public void setBioTemplate(String BioTemplate) {
		this.BioTemplate = BioTemplate;
	}

	@Override
	public String toString() {
		return "ClassPojo [Deleted = " + Deleted + ", ImageID = " + ImageID + ", BioTemplateID = " + BioTemplateID
				+ ", Image = " + Image + ", MatchCount = " + MatchCount + ", Id = " + Id + ", BioTemplate = "
				+ BioTemplate + "]";
	}
}
