package initiations;

import java.io.File;

import common.Execution;
import web.functions.GeneralMethods;

public class TestConfiguration {

	public static String dataLocation = "C:\\posiba_data\\";

	public static File getFile(String mainFile) {
		File file = new File(mainFile);
		if (file.exists()) {
			return file;
		} else {
			File alterFile = new File("project_data//config.properties");
			if (alterFile.exists()) {
				return alterFile;
			} else {
				File alterFile2 = new File(Execution.CONFIGURATION_FILE_PATH + "config.properties");
				return alterFile2;
			}
		}

	}

	static File file = getFile("C:/project_data/config.properties");
	public static String browser;
	/*** Wait time ***/
	public static int waitTime = Integer
			.parseInt(GeneralMethods.getPropertyFileValue(file.getAbsolutePath(), "controlLoadTimeout"));
	public static int pageLoadTimeout = Integer
			.parseInt(GeneralMethods.getPropertyFileValue(file.getAbsolutePath(), "pageLoadTimeout"));

	public static int pageWait = pageLoadTimeout;
	public static int implicitWait = 60;
	public static String apiKey = GeneralMethods.getPropertyFileValue(file.getAbsolutePath(), "api_key");
	public static String screenSolution = GeneralMethods.getPropertyFileValue(file.getAbsolutePath(), "screen_solution");
}
