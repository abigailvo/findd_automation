package objects;

public class BillingInfo {
	private String PerClockPerMonthRate;

	private String Phone;

	private String PerEmployeePerMonthRate;

	private String Email;

	private String ContactName;

	private Address Address;

	private String CompanyOrganization;

	public String getPerClockPerMonthRate() {
		return PerClockPerMonthRate;
	}

	public void setPerClockPerMonthRate(String PerClockPerMonthRate) {
		this.PerClockPerMonthRate = PerClockPerMonthRate;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String Phone) {
		this.Phone = Phone;
	}

	public String getPerEmployeePerMonthRate() {
		return PerEmployeePerMonthRate;
	}

	public void setPerEmployeePerMonthRate(String PerEmployeePerMonthRate) {
		this.PerEmployeePerMonthRate = PerEmployeePerMonthRate;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String Email) {
		this.Email = Email;
	}

	public String getContactName() {
		return ContactName;
	}

	public void setContactName(String ContactName) {
		this.ContactName = ContactName;
	}

	public Address getAddress() {
		return Address;
	}

	public void setAddress(Address Address) {
		this.Address = Address;
	}

	public String getCompanyOrganization() {
		return CompanyOrganization;
	}

	public void setCompanyOrganization(String CompanyOrganization) {
		this.CompanyOrganization = CompanyOrganization;
	}

	@Override
	public String toString() {
		return "ClassPojo [PerClockPerMonthRate = " + PerClockPerMonthRate + ", Phone = " + Phone
				+ ", PerEmployeePerMonthRate = " + PerEmployeePerMonthRate + ", Email = " + Email + ", ContactName = "
				+ ContactName + ", Address = " + Address + ", CompanyOrganization = " + CompanyOrganization + "]";
	}
}
