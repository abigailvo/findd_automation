package methods;

import org.apache.http.HttpResponse;

import initiations.Constants;
import objects.Account;
import objects.Location;
import web.functions.VerifyMethods;

public class ActAccounts {

	public static Account getAccountFromDb(String accountID) {
		Account account = new Account();
		// TODO get information from database
		return account;
	}

	public static void verifyEquals(Account actual, Account expected) {

		VerifyMethods.verifyTextEquals(actual.getName(), expected.getName(), "Verify Account.Name");
		VerifyMethods.verifyTextEquals(actual.getExternalId(), expected.getExternalId(), "Verify Account.ExternalId");
		VerifyMethods.verifyTextEquals(actual.getId(), expected.getId(), "Verify Account.IDs");

		ActBillingInfo.verifyEquals(actual.getBillingInfo(), expected.getBillingInfo());
		ActAccountSettings.verifyEquals(actual.getAccountSettings(), expected.getAccountSettings());
		ActAddress.verifyEquals(actual.getAddress(), expected.getAddress());

		if (actual.getUsers() != null && actual.getUsers().length > 0) {
			for (int i = 0; i < actual.getUsers().length; i++) {
				VerifyMethods.verifyTextEquals(actual.getUsers()[i], expected.getUsers()[i],
						"Verify Account.UserId[" + i + "]");
			}
		}

		if (actual.getLocations() != null && actual.getLocations().length > 0) {
			for (int i = 0; i < actual.getLocations().length; i++) {
				VerifyMethods.verifyTextEquals(actual.getLocations()[i], expected.getLocations()[i],
						"Verify Account.LocationId[" + i + "]");
			}
		}

		if (actual.getAccountUsers() != null && actual.getAccountUsers().length > 0) {
			for (int i = 0; i < actual.getAccountUsers().length; i++) {
				VerifyMethods.verifyTextEquals(actual.getAccountUsers()[i], expected.getAccountUsers()[i],
						"Verify Account.AccountUserId[" + i + "]");
			}
		}
	}

	public static Location apiCreateLocation(String accountId, String cookie) {
		String url = Constants.ACCOUNT_API + accountId + "/locations";
		Location actual = new Location();
		String json = FinddApi.convertObjectToJSON(actual, Location.class);
		HttpResponse response = FinddApi.getResponseFromPOST(url, json, cookie);
		String result = FinddApi.extractJSONFromResponse(response);
		return (Location) FinddApi.convertJSONToObject(result, Location.class);
	}
}
