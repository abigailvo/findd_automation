package initiations;

import java.lang.reflect.Method;
import java.net.MalformedURLException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import common.Execution;
import web.Driver;

public class TestBase {

	@BeforeMethod
	public void beforeBaseMethod(Method method) {
		System.out.println("----------------------\n******** STARTED RUNNING: " + method.getName()
				+ " ********\n----------------------");
		Execution.isPassed = true;
		Execution.method = method;
		Execution.className = this.getClass().getSimpleName();
	}

	@Parameters({ "node", "browser" })
	@BeforeClass
	public void beforeBaseClass(String node, String browser) throws MalformedURLException {
		
		/** RUN TEST ON LOCAL **/
		if (node.equalsIgnoreCase("localhost")) {
			// DRIVER = _driver.get(browser);
			Driver.initBrowser(browser);
		}
		/** RUN TEST ON SELENIUM GRID **/
		else {
			// DRIVER = _driver.get(node, browser);
			Driver.initBrowser(node, browser);
		}

		TestConfiguration.browser = browser;
		Execution.configuration = browser;

		InitializeData();

		Execution.initialize();
	}

	@AfterClass
	public void afterBaseClass() {
		Driver.close();
	}

	private void InitializeData() {

		// Appliumtool Settings
		// Execution.apiKey = TestConfiguration.apiKey;
		// Execution.screenSolution = TestConfiguration.screenSolution;

		// Settings for testrail
		// Execution.keys = Constants.KEYS;
		// Execution.components = Constants.COMPONENTS;
		// Execution.projectId = TestConfiguration.projectId;
		// Execution.planName = TestConfiguration.planName;
		// Execution.clientId = TestConfiguration.clientId;
		// Execution.testrailUsername = TestConfiguration.username;
		// Execution.testrailPassword = TestConfiguration.password;

		Execution.pageWait = TestConfiguration.pageWait;
		Execution.pageWait = TestConfiguration.implicitWait;
		Execution.waitTime = TestConfiguration.waitTime;

	}
}
