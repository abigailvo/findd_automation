package methods;

import java.util.ArrayList;
import java.util.List;

import objects.Address;
import objects.Location;
import web.functions.GeneralMethods;
import web.functions.VerifyMethods;

public class ActLocations {

	public static Location getLocationFromDb(String locationId) {
		Location location = new Location();
		// TODO get information from database
		return location;
	}

	public static List<Location> getLocationsFromDb(String accountID) {
		List<Location> account = new ArrayList<Location>();
		// TODO get information from database
		return account;
	}

	public static void verifyEquals(Location actual, Location expected) {
		VerifyMethods.verifyTextEquals(actual.getId(), expected.getId(), "Verify Location.Id");
		VerifyMethods.verifyTextEquals(actual.getName(), expected.getName(), "Verify Location.Name");
		VerifyMethods.verifyTextEquals(actual.getExternalId(), expected.getExternalId(), "Verify Location.ExternalId");
		VerifyMethods.verifyTextEquals(actual.getTimezoneId(), expected.getTimezoneId(), "Verify Location.TimezoneId");
		VerifyMethods.verifyEquals(actual.getDeleted(), expected.getDeleted(), "Verify Location.Deleted");
		VerifyMethods.verifyEquals(actual.getGeoFencePerimeter(), expected.getGeoFencePerimeter(),
				"Verify Location.GeoFencePerimeter");
		VerifyMethods.verifyEquals(actual.getFloorPlans(), expected.getFloorPlans(), "Verify Location.FloorPlans");
		ActAddress.verifyEquals(actual.getAddress(), expected.getAddress());
		
	}

	public static void verifyEquals(List<Location> actual, List<Location> expected) {
		for (int i = 0; i < actual.size(); i++) {
			verifyEquals(actual.get(i), expected.get(i));
		}
	}
}
