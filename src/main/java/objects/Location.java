package objects;

import java.util.List;

import initiations.Constants;
import methods.FinddApi;
import web.functions.GeneralMethods;

public class Location {
	private String Name;

	private boolean Deleted;

	private String TimezoneId;

	private String ExternalId;

	private Address Address;

	// TODO
	private List<String> FloorPlans;

	private String Id;

	private Integer GeoFencePerimeter;

	public String getName() {
		return Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}

	public boolean getDeleted() {
		return Deleted;
	}

	public void setDeleted(boolean Deleted) {
		this.Deleted = Deleted;
	}

	public String getTimezoneId() {
		return TimezoneId;
	}

	public void setTimezoneId(String TimezoneId) {
		this.TimezoneId = TimezoneId;
	}

	public String getExternalId() {
		return ExternalId;
	}

	public void setExternalId(String ExternalId) {
		this.ExternalId = ExternalId;
	}

	public Address getAddress() {
		return Address;
	}

	public void setAddress(Address Address) {
		this.Address = Address;
	}

	public List<String> getFloorPlans() {
		return FloorPlans;
	}

	public void setFloorPlans(List<String> FloorPlans) {
		this.FloorPlans = FloorPlans;
	}

	public String getId() {
		return Id;
	}

	public void setId(String Id) {
		this.Id = Id;
	}

	public Integer getGeoFencePerimeter() {
		return GeoFencePerimeter;
	}

	public void setGeoFencePerimeter(Integer GeoFencePerimeter) {
		this.GeoFencePerimeter = GeoFencePerimeter;
	}

	@Override
	public String toString() {
		return "ClassPojo [Name = " + Name + ", Deleted = " + Deleted + ", TimezoneId = " + TimezoneId
				+ ", ExternalId = " + ExternalId + ", Address = " + Address + ", FloorPlans = " + FloorPlans + ", Id = "
				+ Id + ", GeoFencePerimeter = " + GeoFencePerimeter + "]";
	}

	public Location() {
		this.Id = FinddApi.generateGUID();
		String randomString = FinddApi.randomStringFromGUID(this.Id);
		this.Name = "QA Test LocationName " + randomString;
		this.Address = new Address(randomString);
		this.ExternalId = randomString;

		this.FloorPlans = null;
		this.Deleted = false;
		this.GeoFencePerimeter = 50;

		int index = GeneralMethods.randomInRange(0, 9);
		this.TimezoneId = Constants.TIMEZONEID[index];
	}
}
