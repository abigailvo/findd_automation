package objects;

public class BiometricSettings {
	private String DefaultThreshold;

	private String DefaultFaceSpaceSetCount;

	public String getDefaultThreshold() {
		return DefaultThreshold;
	}

	public void setDefaultThreshold(String DefaultThreshold) {
		this.DefaultThreshold = DefaultThreshold;
	}

	public String getDefaultFaceSpaceSetCount() {
		return DefaultFaceSpaceSetCount;
	}

	public void setDefaultFaceSpaceSetCount(String DefaultFaceSpaceSetCount) {
		this.DefaultFaceSpaceSetCount = DefaultFaceSpaceSetCount;
	}

	@Override
	public String toString() {
		return "ClassPojo [DefaultThreshold = " + DefaultThreshold + ", DefaultFaceSpaceSetCount = "
				+ DefaultFaceSpaceSetCount + "]";
	}
}
