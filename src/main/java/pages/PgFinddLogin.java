package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import web.functions.GeneralMethods;

public class PgFinddLogin {
	public static WebElement txtEmail() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'useremail')]"));
	}
	
	public static WebElement txtPassword() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'userpassword')]"));
	}
	
	public static WebElement btnLogin() {
		return GeneralMethods.getElement(By.xpath("//button[contains(.,'Login')]"));
	}
	
	public static void launch(){
		GeneralMethods.launch("https://test.findd.io/");
	}
}
