package objects;

public class Logs {
	private String Log;

	private String Tag;

	private String CreationDate; // 2016-10-16T06:32:28.3164732-06:00

	public String getLog() {
		return Log;
	}

	public void setLog(String Log) {
		this.Log = Log;
	}

	public String getTag() {
		return Tag;
	}

	public void setTag(String Tag) {
		this.Tag = Tag;
	}

	public String getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(String CreationDate) {
		this.CreationDate = CreationDate;
	}

	@Override
	public String toString() {
		return "ClassPojo [Log = " + Log + ", Tag = " + Tag + ", CreationDate = " + CreationDate + "]";
	}
}
