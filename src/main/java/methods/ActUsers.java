package methods;

import java.util.ArrayList;
import java.util.List;

import objects.Address;
import objects.BiometricSettings;
import objects.Location;
import objects.User;
import objects.UserRole;
import web.functions.GeneralMethods;
import web.functions.VerifyMethods;

public class ActUsers {

	public static User getUserFromDb(String userId) {
		User user = new User();
		// TODO get information from database
		return user;
	}

	public static List<User> getUsersFromDb(String accountID) {
		List<User> users = new ArrayList<User>();
		// TODO get information from database
		return users;
	}

	public static void verifyEquals(User actual, User expected) {
		VerifyMethods.verifyTextEquals(actual.getId(), expected.getId(), "Verify User.Id");
		VerifyMethods.verifyTextEquals(actual.getEmail(), expected.getEmail(), "Verify User.Email");
		VerifyMethods.verifyTextEquals(actual.getPasswordUpdate(), expected.getPasswordUpdate(),
				"Verify User.PasswordUpdate");
		VerifyMethods.verifyTextEquals(actual.getFirstName(), expected.getFirstName(), "Verify User.FirstName");
		VerifyMethods.verifyTextEquals(actual.getLastName(), expected.getLastName(), "Verify User.LastName");
		VerifyMethods.verifyTextEquals(actual.getExternalId(), expected.getExternalId(), "Verify User.ExternalId");
		VerifyMethods.verifyTextEquals(actual.getClockPin(), expected.getClockPin(), "Verify User.ClockPin");
		VerifyMethods.verifyTextEquals(actual.getProfileImage(), expected.getProfileImage(),
				"Verify User.ProfileImage");
		VerifyMethods.verifyTextEquals(actual.getProfileImageID(), expected.getProfileImageID(),
				"Verify User.ProfileImageID");
		VerifyMethods.verifyTextEquals(actual.getBiometricTrainingDate(), expected.getBiometricTrainingDate(),
				"Verify User.BiometricTrainingDate");
		VerifyMethods.verifyEquals(actual.getDeleted(), expected.getDeleted(), "Verify User.Deleted");
		VerifyMethods.verifyEquals(actual.getAccountUser(), expected.getAccountUser(), "Verify User.AccountUser");

		ActUserRoles.verifyEquals(actual.getUserRole(), expected.getUserRole());
		ActBiometricSettings.verifyEquals(actual.getBiometricSettings(), expected.getBiometricSettings());
		ActAddress.verifyEquals(actual.getAddress(), expected.getAddress());
		ActFaceSpaces.verifyEquals(actual.getFaceSpaces(), expected.getFaceSpaces());
		VerifyMethods.verifyEquals(actual.getLocationIds(), actual.getLocationIds(), "Verify User.LocationIds");
	}

	public static void verifyEquals(List<User> actual, List<User> expected) {
		for (int i = 0; i < actual.size(); i++) {
			verifyEquals(actual.get(i), expected.get(i));
		}
	}
}
