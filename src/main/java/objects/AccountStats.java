package objects;

public class AccountStats {
	private String CurrentMonthEmployeeSubTotal;

	private String CurrentMonthBillingTotal;

	private String[] TotalActiveClockDevicesPast12Months;

	private String YTDBillingTotal;

	private String CurrentMonthClockSubTotal;

	private String[] TotalActiveEmployeesPast12Months;

	private String CurrentMonth;

	private String CurrentMonthTotalActiveEmployees;

	private String CurrentMonthTotalActiveClocks;

	public String getCurrentMonthEmployeeSubTotal() {
		return CurrentMonthEmployeeSubTotal;
	}

	public void setCurrentMonthEmployeeSubTotal(String CurrentMonthEmployeeSubTotal) {
		this.CurrentMonthEmployeeSubTotal = CurrentMonthEmployeeSubTotal;
	}

	public String getCurrentMonthBillingTotal() {
		return CurrentMonthBillingTotal;
	}

	public void setCurrentMonthBillingTotal(String CurrentMonthBillingTotal) {
		this.CurrentMonthBillingTotal = CurrentMonthBillingTotal;
	}

	public String[] getTotalActiveClockDevicesPast12Months() {
		return TotalActiveClockDevicesPast12Months;
	}

	public void setTotalActiveClockDevicesPast12Months(String[] TotalActiveClockDevicesPast12Months) {
		this.TotalActiveClockDevicesPast12Months = TotalActiveClockDevicesPast12Months;
	}

	public String getYTDBillingTotal() {
		return YTDBillingTotal;
	}

	public void setYTDBillingTotal(String YTDBillingTotal) {
		this.YTDBillingTotal = YTDBillingTotal;
	}

	public String getCurrentMonthClockSubTotal() {
		return CurrentMonthClockSubTotal;
	}

	public void setCurrentMonthClockSubTotal(String CurrentMonthClockSubTotal) {
		this.CurrentMonthClockSubTotal = CurrentMonthClockSubTotal;
	}

	public String[] getTotalActiveEmployeesPast12Months() {
		return TotalActiveEmployeesPast12Months;
	}

	public void setTotalActiveEmployeesPast12Months(String[] TotalActiveEmployeesPast12Months) {
		this.TotalActiveEmployeesPast12Months = TotalActiveEmployeesPast12Months;
	}

	public String getCurrentMonth() {
		return CurrentMonth;
	}

	public void setCurrentMonth(String CurrentMonth) {
		this.CurrentMonth = CurrentMonth;
	}

	public String getCurrentMonthTotalActiveEmployees() {
		return CurrentMonthTotalActiveEmployees;
	}

	public void setCurrentMonthTotalActiveEmployees(String CurrentMonthTotalActiveEmployees) {
		this.CurrentMonthTotalActiveEmployees = CurrentMonthTotalActiveEmployees;
	}

	public String getCurrentMonthTotalActiveClocks() {
		return CurrentMonthTotalActiveClocks;
	}

	public void setCurrentMonthTotalActiveClocks(String CurrentMonthTotalActiveClocks) {
		this.CurrentMonthTotalActiveClocks = CurrentMonthTotalActiveClocks;
	}

	@Override
	public String toString() {
		return "ClassPojo [CurrentMonthEmployeeSubTotal = " + CurrentMonthEmployeeSubTotal
				+ ", CurrentMonthBillingTotal = " + CurrentMonthBillingTotal
				+ ", TotalActiveClockDevicesPast12Months = " + TotalActiveClockDevicesPast12Months
				+ ", YTDBillingTotal = " + YTDBillingTotal + ", CurrentMonthClockSubTotal = "
				+ CurrentMonthClockSubTotal + ", TotalActiveEmployeesPast12Months = " + TotalActiveEmployeesPast12Months
				+ ", CurrentMonth = " + CurrentMonth + ", CurrentMonthTotalActiveEmployees = "
				+ CurrentMonthTotalActiveEmployees + ", CurrentMonthTotalActiveClocks = "
				+ CurrentMonthTotalActiveClocks + "]";
	}
}