package initiations;

public class Constants {

	public static String FINDD_URL = "https://test.findd.io/";
	public static String FINDD_API = "https://test-api.findd.io/v1/";
	public static String EMPLOYEE_URL = FINDD_URL + "#/employees";
	public static String ACCOUNT_API = FINDD_API + "accounts/";
	public static String LOCATION_API = FINDD_API + "locations/";
	public static String USER_API = FINDD_API + "users/";
	public static String ACCOUNT_ID = "93ddd288-08c3-439f-8514-4156ed1e4b72";

	public static String[] TIMEZONEID = new String[] { "0", "110", "200", "300", "400", "410", "500", "510", "520" };

	public static class DeviceType {
		public static String CLOCK = "0";
		public static String EMPLOYEE = "1";
		public static String UNKNOWN = "2";
	}
}
