package methods;

import objects.Address;
import web.functions.VerifyMethods;

public class ActAddress {

	public static void verifyEquals(Address actual, Address expected) {
		VerifyMethods.verifyTextEquals(actual.getStreetNumber(), expected.getStreetNumber(),
				"Verify Address.StreetNumber");
		VerifyMethods.verifyTextEquals(actual.getHouseOrBuildingName(), expected.getHouseOrBuildingName(),
				"Verify Address.HouseOrBuildingName");
		VerifyMethods.verifyTextEquals(actual.getStreetNumberSuffix(), expected.getStreetNumberSuffix(),
				"Verify Address.StreetNumberSuffix");
		VerifyMethods.verifyTextEquals(actual.getStreetName(), expected.getStreetName(), "Verify Address.StreetName");
		VerifyMethods.verifyTextEquals(actual.getStreetType(), expected.getStreetType(), "Verify Address.StreetType");
		VerifyMethods.verifyTextEquals(actual.getStreetDirection(), expected.getStreetDirection(),
				"Verify Address.StreetDirection");
		VerifyMethods.verifyTextEquals(actual.getAddressType(), expected.getAddressType(),
				"Verify Address.AddressType");
		VerifyMethods.verifyTextEquals(actual.getLocalMunicipality(), expected.getLocalMunicipality(),
				"Verify Address.LocalMunicipality");
		VerifyMethods.verifyTextEquals(actual.getCityTown(), expected.getCityTown(), "Verify Address.CityTown");
		VerifyMethods.verifyTextEquals(actual.getGoverningDistrictOrState(), expected.getGoverningDistrictOrState(),
				"Verify Address.GoverningDistrictOrState");
		VerifyMethods.verifyTextEquals(actual.getPostalArea(), expected.getPostalArea(), "Verify Address.PostalArea");
		VerifyMethods.verifyTextEquals(actual.getCountry(), expected.getCountry(), "Verify Address.Country");
		VerifyMethods.verifyTextEquals(actual.getPhone(), expected.getPhone(), "Verify Address.Phone");
		VerifyMethods.verifyTextEquals(actual.getMobilePhone(), expected.getMobilePhone(),
				"Verify Address.Mobilephone");
		VerifyMethods.verifyTextEquals(actual.getLatitude(), expected.getLatitude(), "Verify Address.Latitude");
		VerifyMethods.verifyTextEquals(actual.getLongitude(), expected.getLongitude(), "Verify Address.Longtitude");
	}
}
