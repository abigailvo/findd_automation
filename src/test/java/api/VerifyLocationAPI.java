package api;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import initiations.APITestBase;
import initiations.Constants;
import methods.ActAccounts;
import methods.ActDevices;
import methods.ActEmployee;
import methods.ActLocations;
import methods.ActMicroLocations;
import methods.ActUsers;
import methods.FinddApi;
import objects.Device;
import objects.Location;
import objects.MicroLocation;
import objects.User;
import web.functions.VerifyMethods;

public class VerifyLocationAPI extends APITestBase {

	public String cookie = "";
	public String locationAPI = Constants.LOCATION_API;
	Location location = null;
	User user = null;
	Device device = null;
	MicroLocation mLocation = null;

	@BeforeMethod
	public void beforeMethod() {
		cookie = ActEmployee.getCookieFromLoginAPI();
		location = new Location();
		user = new User();
		device = new Device(location.getId());
		mLocation = new MicroLocation(location.getExternalId());
	}

	/**
	 * Add new location. api/Location
	 */
	@Test(priority = 1)
	public void API28_POST_add_new_location() {
		String api = Constants.LOCATION_API + "location";
		String createLocJSON = FinddApi.convertObjectToJSON(location, Location.class);
		HttpResponse response = FinddApi.getResponseFromPOST(api, createLocJSON, cookie);
		String code = FinddApi.getStatusCodeFromResponse(response);

		VerifyMethods.verifyTextEquals(code, "200", "Verify Status of Adding new location");
		VerifyMethods.verifyTest();
	}

	/**
	 * Get location details. locations/{locationId}
	 */
	@Test(priority = 2)
	public void API18_GET_location_detail() {
		// TODO: get another location since API28 gets 500 error
		location = ActAccounts.apiCreateLocation(Constants.ACCOUNT_ID, cookie);
		
		String api = Constants.LOCATION_API + location.getId();
		HttpResponse response = FinddApi.getResponseFromGET(api, cookie);
		String resJson = FinddApi.extractJSONFromResponse(response);
		Location locExpected = (Location) FinddApi.convertJSONToObject(resJson, Location.class);
		ActLocations.verifyEquals(location, locExpected);
		VerifyMethods.verifyTest();
	}

	/**
	 * Get location info by location ID Depreciated. Use GET
	 * locations/{locationId} instead. locations/{locationId}/info
	 */
	//@Test(priority = 3)
	public void API19_GET_location_detail() {
		String api = Constants.LOCATION_API + location.getId() + "/info";
		HttpResponse response = FinddApi.getResponseFromGET(api, cookie);
		String resJson = FinddApi.extractJSONFromResponse(response);
		Location locExpected = (Location) FinddApi.convertJSONToObject(resJson, Location.class);
		ActLocations.verifyEquals(location, locExpected);
		VerifyMethods.verifyTest();
	}

	/**
	 * Associate user to location by location id and user id.
	 * locations/{locationId}/users/add?userId={userId}
	 */
	@Test(priority = 4)
	public void API17_POST_add_user_to_location() {
		String url = locationAPI + "%locationId%/users/add?userId=%userId%";
		url = url.replace("%locationId%", location.getId()).replaceAll("%userId%", user.getId());

		HttpResponse response = FinddApi.getResponseFromPOST(url, "", cookie);
		String code = FinddApi.getStatusCodeFromResponse(response);
		VerifyMethods.verifyTextEquals(code, "200", "Verify Response Code of adding user to location");

		VerifyMethods.verifyTest();
	}

	/**
	 * Get users associated with location. List can be empty.
	 * locations/{locationId}/users?includeFaceSpaceTemplates={includeFaceSpaceTemplates}
	 */
	@Test(priority = 5)
	public void API16_GET_users_associate_with_location() {
		List<User> actual = new ArrayList<User>();
		actual.add(user);

		String api = Constants.LOCATION_API + location.getId() + "/users";
		HttpResponse response = FinddApi.getResponseFromGET(api, cookie);
		String resJson = FinddApi.extractJSONFromResponse(response);
		List<User> expected = FinddApi.convertJsonToObjectArray(resJson, User[].class);
		ActUsers.verifyEquals(actual, expected);
		VerifyMethods.verifyTest();
	}

	/**
	 * Add a device to a location locations/{locationId}/devices
	 */
	@Test(priority = 6)
	public void API25_POST_Add_a_device_to_location() {
		String api = Constants.LOCATION_API + location.getId() + "/devices";

		String sendJson = FinddApi.convertObjectToJSON(device, Device.class);
		HttpResponse resp = FinddApi.getResponseFromPOST(api, sendJson, cookie);

		VerifyMethods.verifyTextEquals(FinddApi.getStatusCodeFromResponse(resp), "200",
				"Verify Status code of adding device to location");
		VerifyMethods.verifyTest();
	}

	/**
	 * Get devices assigned to a location locations/{locationId}/devices
	 */
	@Test(priority = 7, enabled = false)
	public void API20_GET_device_assigned_to_location() {
		List<Device> actual = new ArrayList<Device>();
		actual.add(device);

		String api = Constants.LOCATION_API + location.getId() + "/devices";
		HttpResponse response = FinddApi.getResponseFromGET(api, cookie);
		String respJson = FinddApi.extractJSONFromResponse(response);
		List<Device> expected = FinddApi.convertJsonToObjectArray(respJson, Device[].class);
		ActDevices.verifyEquals(actual, expected);
		VerifyMethods.verifyTest();
	}

	/**
	 * Remove device from location. locations/{locationId}/devices/{deviceId}
	 */
	@Test(priority = 8, enabled = false)
	public void API21_DELETE_remove_devices_from_location() {
		String api = Constants.LOCATION_API + location.getId() + "/devices/" + device.getId();
		HttpResponse response = FinddApi.getResponseFromDELETE(api, cookie);
		String code = FinddApi.getStatusCodeFromResponse(response);
		VerifyMethods.verifyTextEquals(code, "204");
		VerifyMethods.verifyTest();
	}

	/**
	 * Associate microlocation with location.
	 * locations/{locationId}/microlocations
	 */
	@Test(priority = 9, enabled = false)
	public void API24_POST_Associate_microlocation_with_location() {
		String api = Constants.LOCATION_API + location.getId() + "/microlocations";

		String sendJson = FinddApi.convertObjectToJSON(mLocation, MicroLocation.class);
		HttpResponse resp = FinddApi.getResponseFromPOST(api, sendJson, cookie);

		VerifyMethods.verifyTextEquals(FinddApi.getStatusCodeFromResponse(resp), "200",
				"Verify Status code of adding microlocation to location");
		VerifyMethods.verifyTest();
	}

	/**
	 * Retrieve all MicroLocations that are assigned to a location.
	 * locations/{locationId}/microlocations
	 */
	@Test(priority = 10, enabled = false)
	public void API22_GET_retrieve_microlocation_assigned_to_location() {
		List<MicroLocation> actual = new ArrayList<MicroLocation>();
		actual.add(mLocation);

		String api = Constants.LOCATION_API + location.getId() + "/microlocations";
		HttpResponse response = FinddApi.getResponseFromGET(api, cookie);
		String respJson = FinddApi.extractJSONFromResponse(response);
		List<MicroLocation> expected = FinddApi.convertJsonToObjectArray(respJson, MicroLocation[].class);
		ActMicroLocations.verifyEquals(actual, expected);
		VerifyMethods.verifyTest();
	}

	/**
	 * Remove microlocation from location.
	 * locations/{locationId}/microlocations/{microlocationId}
	 */
	@Test(priority = 11, enabled = false)
	public void API23_DELETE_remove_microlocation_from_location() {
		String api = Constants.LOCATION_API + location.getId() + "/microlocations/" + mLocation.getId();
		HttpResponse response = FinddApi.getResponseFromDELETE(api, cookie);
		String code = FinddApi.getStatusCodeFromResponse(response);
		VerifyMethods.verifyTextEquals(code, "204");
		VerifyMethods.verifyTest();
	}

	/**
	 * Retrieve records by location. locations/{locationId}/records
	 */
	public void API26_GET_retrieve_records_by_location() {

	}

	/**
	 * locations/{locationId}
	 */
	public void API14_PATCH_location() {

	}

	/**
	 * Update location api/Location?locationId={locationId}
	 */
	public void API27_PUT_update_location() {

	}

	/**
	 * Marks location as deleted, removes location from users and account.
	 * locations/{locationId}
	 */
	public void API15_DELETE_location() {
		String api = Constants.LOCATION_API + location.getId();
		HttpResponse response = FinddApi.getResponseFromDELETE(api, cookie);
		String code = FinddApi.getStatusCodeFromResponse(response);
		VerifyMethods.verifyTextEquals(code, "204");
		VerifyMethods.verifyTest();
	}
}
