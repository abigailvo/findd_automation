package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import initiations.Constants;
import web.functions.GeneralMethods;

/**
 * Sample page
 */
public class PgEmployeeList {
	public static WebElement btnAddEmployee() {
		return GeneralMethods.getElement(By.xpath("//div[@class='fixed-add']//button"));
	}

	public static WebElement btnSearch() {
		return GeneralMethods.getElement(By.xpath("//i[contains(@class,'fa fa-search itemActionButton')]/parent::div"));
	}
	
	public static WebElement txtSearch() {
		return GeneralMethods.getElement(By.xpath("//input[contains(@id,'search')]"));
	}
	
	public static WebElement btnHideSearchTextbox() {
		return GeneralMethods.getElement(By.xpath("//i[contains(@class,'fa fa-close itemActionButton')]/parent::div"));
	}
	
	public static WebElement btnDeleteEmployees() {
		return GeneralMethods.getElement(By.xpath("//i[contains(@class,'fa fa-trash itemActionButton')]/parent::div"));
	}
	
	public static WebElement chkSelectAllEmployees(String clockpin) {
		return GeneralMethods.getElement(By.xpath("//th[.='Clock Pin']/preceding-sibling::th[5]"));
	}
	
	/**
	 * return a row of employee by clockpin
	 * @param clockpin a unique string for each employee
	 * @return
	 */
	public static WebElement rowEmployee(String clockpin) {
		return GeneralMethods.getElement(By.xpath("//td[.='" + clockpin + "']/parent::tr"));
	}
	
	public static WebElement chkEmployeeCheckbox(String clockpin) {
		return GeneralMethods.getElement(By.xpath("//td[.='" + clockpin + "']/preceding-sibling::td[5]"));
	}
	
	public static WebElement colEmployeeFirstname(String clockpin) {
		return GeneralMethods.getElement(By.xpath("//td[.='" + clockpin + "']/preceding-sibling::td[5]"));
	}
	
	public static WebElement colEmployeeLastname(String clockpin) {
		return GeneralMethods.getElement(By.xpath("//td[.='" + clockpin + "']/preceding-sibling::td[5]"));
	}
	
	public static WebElement colEmployeeTrainedDate(String clockpin) {
		return GeneralMethods.getElement(By.xpath("//td[.='" + clockpin + "']/preceding-sibling::td[5]"));
	}
	
	public static WebElement colEmployeeExternalID(String clockpin) {
		return GeneralMethods.getElement(By.xpath("//td[.='" + clockpin + "']/preceding-sibling::td[5]"));
	}
	
	public static WebElement colEmployeeClockpin(String clockpin) {
		return GeneralMethods.getElement(By.xpath("//td[.='" + clockpin + "']/preceding-sibling::td[5]"));
	}

	public static void launch() {
		GeneralMethods.launch(Constants.EMPLOYEE_URL);
	}
}
