package objects;

import java.util.List;

import methods.FinddApi;

public class User {

	public User() {
		this.Id = FinddApi.generateGUID();
		String randomString = FinddApi.randomStringFromGUID(this.Id);
		this.Email = "upwork." + randomString + "@testemail.com";
		this.PasswordUpdate = null;
		this.UserRole = new UserRole();
		this.FirstName = "QA Test FN " + randomString;
		this.LastName = "QA Test LN " + randomString;
		this.ExternalId = "EX" + randomString.toUpperCase();
		this.ClockPin = "CL" + randomString;
		this.BiometricSettings = new BiometricSettings();
		this.Address = new Address(randomString);
		this.FaceSpaces = null;
		this.LocationIds = null;
		this.ProfileImage = null;
		this.ProfileImageID = null;
		this.BiometricTrainingDate = FinddApi.getCurrentDateWithFinddFormat();
		this.Deleted = false;
		this.AccountUser = false;
	}

	private String ProfileImageID;

	private BiometricSettings BiometricSettings;

	private boolean Deleted;

	private List<FaceSpaces> FaceSpaces;

	private String ExternalId;

	private String ProfileImage;

	private List<String> LocationIds;

	private String BiometricTrainingDate;

	private String LastName;

	private String ClockPin;

	private UserRole UserRole;

	private String Email;

	private Address Address;

	private String FirstName;

	private String Id;

	/**
	 * Flag to indicate whether or not a user is an account user.
	 */
	private boolean AccountUser;

	private String PasswordUpdate;

	public String getProfileImageID() {
		return ProfileImageID;
	}

	public void setProfileImageID(String ProfileImageID) {
		this.ProfileImageID = ProfileImageID;
	}

	public BiometricSettings getBiometricSettings() {
		return BiometricSettings;
	}

	public void setBiometricSettings(BiometricSettings BiometricSettings) {
		this.BiometricSettings = BiometricSettings;
	}

	public boolean getDeleted() {
		return Deleted;
	}

	public void setDeleted(boolean Deleted) {
		this.Deleted = Deleted;
	}

	public List<FaceSpaces> getFaceSpaces() {
		return FaceSpaces;
	}

	public void setFaceSpaces(List<FaceSpaces> FaceSpaces) {
		this.FaceSpaces = FaceSpaces;
	}

	public String getExternalId() {
		return ExternalId;
	}

	public void setExternalId(String ExternalId) {
		this.ExternalId = ExternalId;
	}

	public String getProfileImage() {
		return ProfileImage;
	}

	public void setProfileImage(String ProfileImage) {
		this.ProfileImage = ProfileImage;
	}

	public List<String> getLocationIds() {
		return LocationIds;
	}

	public void setLocationIds(List<String> LocationIds) {
		this.LocationIds = LocationIds;
	}

	public String getBiometricTrainingDate() {
		return BiometricTrainingDate;
	}

	public void setBiometricTrainingDate(String BiometricTrainingDate) {
		this.BiometricTrainingDate = BiometricTrainingDate;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String LastName) {
		this.LastName = LastName;
	}

	public String getClockPin() {
		return ClockPin;
	}

	public void setClockPin(String ClockPin) {
		this.ClockPin = ClockPin;
	}

	public UserRole getUserRole() {
		return UserRole;
	}

	public void setUserRole(UserRole UserRole) {
		this.UserRole = UserRole;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String Email) {
		this.Email = Email;
	}

	public Address getAddress() {
		return Address;
	}

	public void setAddress(Address Address) {
		this.Address = Address;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String FirstName) {
		this.FirstName = FirstName;
	}

	public String getId() {
		return Id;
	}

	public void setId(String Id) {
		this.Id = Id;
	}

	public boolean getAccountUser() {
		return AccountUser;
	}

	public void setAccountUser(boolean AccountUser) {
		this.AccountUser = AccountUser;
	}

	public String getPasswordUpdate() {
		return PasswordUpdate;
	}

	public void setPasswordUpdate(String PasswordUpdate) {
		this.PasswordUpdate = PasswordUpdate;
	}

	@Override
	public String toString() {
		return "ClassPojo [ProfileImageID = " + ProfileImageID + ", BiometricSettings = " + BiometricSettings
				+ ", Deleted = " + Deleted + ", FaceSpaces = " + FaceSpaces + ", ExternalId = " + ExternalId
				+ ", ProfileImage = " + ProfileImage + ", LocationIds = " + LocationIds + ", BiometricTrainingDate = "
				+ BiometricTrainingDate + ", LastName = " + LastName + ", ClockPin = " + ClockPin + ", UserRole = "
				+ UserRole + ", Email = " + Email + ", Address = " + Address + ", FirstName = " + FirstName + ", Id = "
				+ Id + ", AccountUser = " + AccountUser + ", PasswordUpdate = " + PasswordUpdate + "]";
	}
}