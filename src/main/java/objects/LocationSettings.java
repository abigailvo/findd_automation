package objects;

public class LocationSettings {
	private String DefaultGeoFencePerimeter;

	private String DefaultMapLongitude;

	private String DefaultMapLatitude;

	private String DefaultMicroLocationPerimeter;

	public String getDefaultGeoFencePerimeter() {
		return DefaultGeoFencePerimeter;
	}

	public void setDefaultGeoFencePerimeter(String DefaultGeoFencePerimeter) {
		this.DefaultGeoFencePerimeter = DefaultGeoFencePerimeter;
	}

	public String getDefaultMapLongitude() {
		return DefaultMapLongitude;
	}

	public void setDefaultMapLongitude(String DefaultMapLongitude) {
		this.DefaultMapLongitude = DefaultMapLongitude;
	}

	public String getDefaultMapLatitude() {
		return DefaultMapLatitude;
	}

	public void setDefaultMapLatitude(String DefaultMapLatitude) {
		this.DefaultMapLatitude = DefaultMapLatitude;
	}

	public String getDefaultMicroLocationPerimeter() {
		return DefaultMicroLocationPerimeter;
	}

	public void setDefaultMicroLocationPerimeter(String DefaultMicroLocationPerimeter) {
		this.DefaultMicroLocationPerimeter = DefaultMicroLocationPerimeter;
	}

	@Override
	public String toString() {
		return "ClassPojo [DefaultGeoFencePerimeter = " + DefaultGeoFencePerimeter + ", DefaultMapLongitude = "
				+ DefaultMapLongitude + ", DefaultMapLatitude = " + DefaultMapLatitude
				+ ", DefaultMicroLocationPerimeter = " + DefaultMicroLocationPerimeter + "]";
	}
}
